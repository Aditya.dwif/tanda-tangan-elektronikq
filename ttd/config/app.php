<?php

return [
    'locale'    => [
        // The application's default localization/language
        'default'   => env('APP_LOCALE_DEFAULT'),
        // The locale used by the app in case the default is left out
        'fallback'   => env('APP_LOCALE_FALLBACK'),
    ],
    'storage' => str_replace("config", "uploads/", dirname(__FILE__)),
    'IS_DEMO' => false, // true, false
    'OTP_AKTIF' => false, // true, false

    'metadata' => [
        'title' => env('APP_NAME', 'Digital Signature'),
        'title_desc' => env('APP_NAME', 'Digital Signature'),
    ],


    'ATTCHMENT_MAX_PER_FILE' => env('ATTCHMENT_MAX_PER_FILE'),
    'ATTCHMENT_MAX_PER_UPLOAD' => env('ATTCHMENT_MAX_PER_UPLOAD'),
];
