var a_loader = '<div class="loader-box"><div class="circle-loader"></div></div>';

$(document).ready(function($) {
    /*
    $('#table_templates').DataTable({
        "fnDrawCallback": function ( oSettings ) {
            $(oSettings.nTHead).hide();
        },
        // "lengthChange": false,
        "pageLength": 100,
        // "bInfo": false,
        // "scrollY": "150px",
        // "scrollCollapse": true,
        // "paging":   false,
        // "dom": '<"top"f><"clear"><"bottom"p>',
        // "scrollY": "20px",
        // "fnInitComplete" : function(oSettings, json) {
        //     $('.dataTables_scrollHead').height(1);
        // }
    });
    */

    $('#table_templates').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            "url": baseUrl+"templates/mytemplate",
            "type": 'POST',
            "data": {
                id: '',
                "csrf-token": Cookies.get("CSRF-TOKEN")
            }
        },
        columns: [
            {data: 'type', name: 'type'},
            {data: 'file', name: 'file'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],

        "fnDrawCallback": function ( oSettings ) {
            $(oSettings.nTHead).hide();
        },
        "pageLength": 100,
    });


});
