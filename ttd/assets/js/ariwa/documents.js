var a_loader = '<div class="loader-box"><div class="circle-loader"></div></div>';

$(document).ready(function($) {
    getDataNeedApproval();
    getDataIsApprove();
});

function getDataNeedApproval() {
    $('#document_need_apprv').load(baseUrl+'documents/needapproval', function() {
        $('#document_need_appr_table').DataTable({
            "fnDrawCallback": function ( oSettings ) {
                $(oSettings.nTHead).hide();
            },
            // "lengthChange": false,
            "pageLength": 100,
            // "bInfo": false,
            // "scrollY": "150px",
            // "scrollCollapse": true,
            // "paging":   false,
            // "dom": '<"top"f><"clear"><"bottom"p>',
            // "scrollY": "20px",
            // "fnInitComplete" : function(oSettings, json) {
            //     $('.dataTables_scrollHead').height(1);
            // }
        });
    });

}
function getDataIsApprove() {
    $('#document_is_approve').load(baseUrl+'documents/myapprove', function() {
        $('#doc_my_approval_table').DataTable({
            "fnDrawCallback": function ( oSettings ) {
                $(oSettings.nTHead).hide();
            },
            // "lengthChange": false,
            "pageLength": 100,
            // "bInfo": false,
            // "scrollY": "150px",
            // "scrollCollapse": true,
            // "dom": '<"top"f><"clear"><"bottom"p>',
            // "scrollY": "20px",
            // "fnInitComplete" : function(oSettings, json) {
            //     $('.dataTables_scrollHead').height(1);
            // }
        });
    });
}
