$(document).ready(function(){
	$('#filer_input').filer({
		showThumbs: true,
		addMore: true,
		allowDuplicates: false,
		limit: 4,
		maxSize: maxPerUpload,
		fileMaxSize: maxPerFile,
		extensions: ["doc", "docx", "gif", "jpg", "jpeg", "png", "pdf", "xls", "xlsx", "ppt", "pptx", "zip", "rar"],
		captions:{
			button: "Choose Files",
			feedback: "Choose files To Upload",
			feedback2: "files were chosen",
			drop: "Drop file here to Upload",
			removeConfirmation: "Are you sure you want to remove this file?",
			errors: {
				filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
				filesType: "File type not allowed",
				filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-fileMaxSize}} MB.",
				filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
				folderUpload: "You are not allowed to upload folders."
			}
		},
	});
});
