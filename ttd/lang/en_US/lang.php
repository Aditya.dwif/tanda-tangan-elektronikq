<?php

return [
    'button'    => [
        'login' => 'Login',
    ],
    'input' => [
        'label'         => [
            'name'  => 'Name',
        ],
        'placeholder'   => [
            'email'     => 'Enter Username or Email',
            'password'  => 'Enter Password',
        ],
    ],
    'title' => [
        'login' => 'Login',
    ],

    'history' => [
        'request' => 'Mengajukan tanda tangan ke'
    ]
];
