<?php
namespace Simcify\Controllers;

use Simcify\File;
use Simcify\Mail;
use Simcify\Auth;
use Simcify\Database;
use Illuminate\Database\Capsule\Manager as DB;

class User{

    /**
     * Get users view
     * 
     * @return \Pecee\Http\Response
     */
    public function get() {
        $users = DB::table("users")
            ->select(['users.*'
                , '_sekertaris.user_sek', 'us2.fname as sek_fname', 'us2.lname as sek_lname'
                , 'us2.phone as sek_phone'
                , '_sekertaris.created_at as created_at_sek'
            ])
            ->leftJoin('_sekertaris', '_sekertaris.user_parent', '=', 'users.email')
            ->leftJoin('users as us2', 'us2.email', '=', 'users.email')
            ->where("users.company", 0)
            ->get();

        $users_setsek = $users;
        $usersData = array();
        foreach ($users as $user) {
            $usersData[] = array(
                                                "user" => $user,
                                                "files" => Database::table("files")->where("uploaded_by" , $user->id)->count("id", "files")[0]->files,
                                                "disk" => Database::table("files")->where("uploaded_by" , $user->id)->sum("size", "size")[0]->size
                                            );
        }
        $user = Auth::user();
        if ($user->role != "superadmin") {
            return view('errors/404');   
        }
        $users = $usersData;
        return view('users', compact("user", "users", "users_setsek"));
    }

    /**
     * Create user account
     * 
     * @return Json
     */
    public function create() {
        header('Content-type: application/json');
        if(config('app.IS_DEMO')){
            exit(json_encode(responder("success", "Hanya Simulasi!", "Tidak berlaku untuk Demo.","reload()")));
        }
        $password = rand(111111, 999999);
        if (!empty(input('avatar'))) {
            $upload = File::upload(
                input('avatar'), 
                "avatar",
                array(
                    "source" => "base64",
                    "extension" => "png"
                )
            );
            $avatar = $upload['info']['name'];
        }else{
            $avatar = '';
        }
        $signup = Auth::signup(
            array(
                "fname" => escape(input('fname')),
                "lname" => escape(input('lname')),
                "phone" => escape(input('phone')),
                "email" => escape(input('email')),
                "address" => escape(input('address')),
                "avatar" => $avatar,
                "role" => "user",
                "company" => 0,
                "password" => Auth::password($password)
            ), 
            array(
                "uniqueEmail" => input('email')
            )
        );
        if ($signup["status"] == "success") {
            Mail::send(
                input('email'),
                "Welcome to ".env("APP_NAME")."!",
                array(
                    "title" => "Welcome to ".env("APP_NAME")."!",
                    "subtitle" => "A new account has been created for you at ".env("APP_NAME").".",
                    "buttonText" => "Login Now",
                    "buttonLink" => env("APP_URL"),
                    "message" => "These are your login Credentials:<br><br><strong>Email:</strong>".input('email')."<br><strong>Password:</strong>".$password."<br><br>Cheers!<br>".env("APP_NAME")." Team."
                ),
                "withbutton"
            );
            exit(json_encode(responder("success", "Account Created", "Account successfully created","reload()")));
        }else{
            if (!empty(($avatar))) {
                File::delete($avatar, "avatar");
            }
            exit(json_encode(responder("error", "Oops!", $signup["message"])));
        }
    }

    /**
     * Delete user account
     * 
     * @return Json
     */
    public function delete() {
        header('Content-type: application/json');
        if(config('app.IS_DEMO')){
            exit(json_encode(responder("success", "Hanya Simulasi!", "Tidak berlaku untuk Demo.","reload()")));
        }
        $account = Database::table("users")->where("id", input("userid"))->first();
        if (!empty($account->avatar)) {
            File::delete($account->avatar, "avatar");
        }
        $deleted = Database::table("users")->where("id", input("userid"))->delete();
        if($deleted){
            exit(json_encode(responder("success", "Account Deleted!", "Account successfully deleted.","reload()")));
        }
        exit(json_encode(responder("error", "Oops!", "Akun tidak bisa dihapus, karena mempunya dokumen.")));
    }

    /**
     * User update view
     * 
     * @return Json
     */
    public function updateview() {
        $data = array(
                "user" => Database::table("users")->where("id", input("userid"))->first()
            );
        return view('extras/updateuser', $data);
    }

    /**
     * Update user account
     * 
     * @return Json
     */
    public function update() {
        header('Content-type: application/json');
        if(config('app.IS_DEMO')){
            exit(json_encode(responder("success", "Hanya Simulasi!", "Tidak berlaku untuk Demo.","reload()")));
        }
        $account = Database::table("users")->where("id", input("userid"))->first();

        foreach (input()->post as $field) {
            if ($field->index == "avatar") {
                if (!empty($field->value)) {
                    $avatar = File::upload(
                        $field->value, 
                        "avatar",
                        array(
                            "source" => "base64",
                            "extension" => "png"
                        )
                    );

                    if ($avatar['status'] == "success") {
                        if (!empty($account->avatar)) {
                            File::delete($account->avatar, "avatar");
                        }
                        Database::table(config('auth.table'))->where("id" , input("userid"))->update(array("avatar" => $avatar['info']['name']));
                    }
                }
                continue;
            }
            if ($field->index == "csrf-token" || $field->index == "userid") {
                continue;
            }
            Database::table(config('auth.table'))->where("id" , input("userid"))->update(array($field->index => escape($field->value)));
        }
        exit(json_encode(responder("success", "Alright", "Account successfully updated","reload()")));
    }

    public function setsekretaris(){
        $user = Auth::user();

        if(input('email') == input('sekemail')){
            die(json_encode(['stts'=>false, 'msg' => 'Sekretaris tidak boleh sama dengan ybs.']));
        }

        $cek = DB::table('_sekertaris')
            ->where('user_parent', input('email'))
            ->where('user_sek', input('sekemail'))
            ->first()
            ;
        if($cek){
            die(json_encode(['stts'=>false, 'msg' => 'Data sudah ada.']));
        }

        // hapus dulu
        DB::table('_sekertaris')
            ->where('user_parent', input('email'))
            ->delete();

        if(input('sekemail'))
            DB::table('_sekertaris')
                ->insert([
                    'user_parent' => input('email'),
                    'user_sek' => input('sekemail'),
                    'created_by' => $user->id,
                ])
                ;
        die(json_encode(['stts'=>true]));
    }

}
