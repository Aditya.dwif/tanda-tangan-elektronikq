<?php
namespace Simcify\Controllers;

use Simcify\Str;
use Simcify\File;
use Simcify\Mail;
use Simcify\Auth;
use Simcify\Signer;
use Simcify\Database;
use Illuminate\Database\Capsule\Manager as DB;

class Request{

    /**
     * Get requests view
     * 
     * @return \Pecee\Http\Response
     */
    public function get() {
        $user = Auth::user();
        if ($user->role == "user") {
        	$requestsData = Database::table("requests")->where("sender", $user->id)->orderBy("id", false)->get();
        }else{
        	$requestsData = Database::table("requests")->where("company", $user->company)->orderBy("id", false)->get();
        }
        $requests = array();
        foreach ($requestsData as $request) {
        	if (!empty($request->receiver)) {
        		$receiver = Database::table("users")->where("id" , $request->receiver)->first();
        		if (!empty($receiver)) {
        			$receiverInfo = $receiver;
        		}
        	}else{
    			$receiver = Database::table("users")->where("email" , $request->email)->first();
        		if (!empty($receiver)) {
        			$receiverInfo = $receiver;
        		}else{
        			$receiverInfo = $request->email;
        		}
    		}
			
            // $receiverInfo->avatar = 'ATrqvnTHCyI73MnWZNTyD9haTMtAU3o6.png';
        	$requests[] = array(
                                                "data" => $request,
                                                "file" => Database::table("files")->where("document_key" , $request->document)->first(),
                                                "sender" => Database::table("users")->where("id" , $request->sender)->first(),
                                                "receiver" => $receiverInfo
                                            );
        }
        
        return view('requests', compact("user", "requests"));
    }

    /**
     * Send signing request
     * 
     * @return Json
     */
    public function send() {
    	header('Content-type: application/json');
    	$user = Auth::user();
    	$document = Database::table("files")->where("document_key", input("document_key"))->first();
    	$emails = json_decode($_POST['emails'], true);
    	$message = $_POST['message'];
    	$documentKey = $document->document_key;
    	$duplicate = $_POST['duplicate'];
    	//$chain = $_POST['chain'];
        // ini untuk membalik fungsi serial menjadi paralel dan sebaliknya
        $chain = $_POST['chain']=='No' ? (count($emails)>1 ? 'Yes' : 'No' ) : 'No';

        $emails = $this->_SetSekertaris($chain, $emails, $document);

        if($chain == "Yes") $serial_paralel = 'Serial'; else $serial_paralel = '';

    	$chainEmails = $chainPositions = '';
    	$activity = 'Mengajukan tanda tangan ke <span class="text-primary">'.implode(", ", $emails).'</span> oleh <span class="text-primary">'.escape($user->fname.' '.$user->lname).'</span>.';
    	if (!empty($_POST['positions']) && $document->is_template == "No") {
	    	$positionsOriginal = json_decode($_POST['positions'], true);
	    	if(count($emails) > 1){
	    	    foreach($positionsOriginal as $pIndex => $positionsSingle){
	    	        $positionsSingle = json_decode($positionsSingle, true);
	    	        if (!empty($positionsSingle)) {
            	    	if (input("docWidth") != "set") { array_unshift($positionsSingle, input("docWidth")); }
            	    	$positions[] = json_encode($positionsSingle);
	    	        }else{
                		$positions[] = '';
                	}
	    	    }
	    	}else{
    	    	$positionsSingle = json_decode($positionsOriginal[0], true);
    	    	if (input("docWidth") != "set") { array_unshift($positionsSingle, input("docWidth")); }
    	    	$positions[0] = json_encode($positionsSingle);
	    	}
    	}elseif(!empty($_POST['positions']) && $document->is_template == "Yes"){
	        foreach($emails as $index => $email){
	            $positions[] = $_POST['positions'];
	        }
    	}else{
    	    foreach($emails as $index => $email){
    	        $positions[] = '';
    	    }
    	}
        if($chain == "Yes"){
            $chainEmails = $emails;
            unset($chainEmails[0]);
            $chainEmails = array_values ( $chainEmails );
            $chainEmails = json_encode($chainEmails);
            $chainPositions = $positions;
            unset($chainPositions[0]);
            $chainPositions = array_values ( $chainPositions );
            $chainPositions = json_encode($chainPositions);
        }
    	foreach($emails as $index => $email){
    		$signingKey = Str::random(32);
    		if ($duplicate == "Yes" || $document->is_template == "Yes") {
    			$duplicateDocId = Signer::duplicate($document->id, $document->name." (".$email.")");
    			$duplicateDoc = Database::table("files")->where("id", $duplicateDocId)->first();
    			$documentKey = $duplicateDoc->document_key;
                Database::table("files")->where("id", $duplicateDoc->id)->update(array("is_template" => "No"));
    			$duplicateActivity = 'Mengajukan tanda tangan ke <span class="text-primary">'.escape($email).'</span> oleh <span class="text-primary">'.escape($user->fname.' '.$user->lname).'</span>.';
    			Signer::keephistory($documentKey, $duplicateActivity, "default");
    		}
    		if( env('GUEST_SIGNING') == "Enabled" AND env('FORCE_GUEST_SIGNING') == "Enabled"){
    		    $signingLink = env("APP_URL")."/view/".$documentKey."?signingKey=".$signingKey;
    		}else{
    		    $signingLink = env("APP_URL")."/document/".$documentKey."?signingKey=".$signingKey;
    		}
			$trackerLink = env("APP_URL")."/mailopen?signingKey=".$signingKey;
			$receiverData = Database::table("users")->where("email", $email)->first();
			if (!empty($receiverData)) { $receiver = $receiverData->id; }else{ $receiver = 0; }
			$request = array( "sender_note" => escape(input("message")),  "chain_emails" => $chainEmails, "chain_positions" => $chainPositions, "company" => $user->company, "document" => $documentKey, "signing_key" => $signingKey, "positions" => $positions[$index], "email" => $email, "sender" => $user->id, "receiver" => $receiver );
			Database::table("requests")->insert($request);
			// add to chat
            $chat = array( "sender" => $user->id, "message" => escape(input("message")), "time_" => date('Y-m-d H:i:s'),  "file" => $documentKey );
            Database::table("chat")->insert($chat);
    		$send = Mail::send(
                $email, $user->fname." ".$user->lname." has invited you to sign a document",
                array(
                    "title" => "Document Signing invite",
                    "subtitle" => "Click the link below to respond to the invite.",
                    "buttonText" => "Sign Now",
                    "buttonLink" => $signingLink,
                    "message" => "You have been invited to sign a document by ".$user->fname." ".$user->lname.". Click the link above to respond to the invite.<br><strong>Message:</strong> ".input("message")."<br><br>Cheers!<br>".env("APP_NAME")." Team
                    <img src='".$trackerLink."' width='0' height='0'>"
                ),
                "withbutton"
            );
            if (!$send) { exit(json_encode(responder("error", "Oops!", $send->ErrorInfo))); }
            if($chain == "Yes"){ break; }
    	}
    	Signer::keephistory($document->document_key, $activity, "default");
    	exit(json_encode(responder("success", "Sent!", "Request successfully sent.","reload()")));
    }

    /**
     * Delete signing request
     * 
     * @return Json
     */
    public function delete() {
    	header('Content-type: application/json');
    	$requestId = input("requestid");
    	Database::table("requests")->where("id", $requestId)->delete();
    	exit(json_encode(responder("success", "Deleted!", "Request successfully deleted.","reload()")));
    }

    /**
     * Cancel signing request
     * 
     * @return Json
     */
    public function cancel() {
    	header('Content-type: application/json');
    	$requestId = input("requestid");
    	 Database::table("requests")->where("id", $requestId)->update(array("status" => "Cancelled", "update_time" => date("Y-m-d H-i-s")));
    	// keep history
        $userreceiver = DB::table('requests as a')
            ->where('a.id', $requestId)
            ->select(['*',
                'b.email',
                'c.email as email_sender'
                ])
            ->selectRaw('concat(c.fname, \' \', c.lname) as name_sender')
            ->leftJoin('users as b', 'a.receiver', '=', 'b.id')
            ->leftJoin('users as c', 'a.sender', '=', 'c.id')
            ->first()
            ;
        $activity = 'Pengajuan tanda tangan ke <span class="text-primary">'.$userreceiver->email.'</span> di batalkan oleh <span class="text-primary">'.$userreceiver->name_sender.'</span>.';
        Signer::keephistory($userreceiver->document, $activity, "default");
    	exit(json_encode(responder("success", "Cancelled!", "Request successfully cancelled.","reload()")));
    }

    /**
     * Send a signing request reminder
     * 
     * @return Json
     */
    public function remind() {
        header('Content-type: application/json');
        $requestId = input("requestid");
        $user = Auth::user();
        $request = Database::table("requests")->where("id", $requestId)->first();
		if( env('GUEST_SIGNING') == "Enabled" AND env('FORCE_GUEST_SIGNING') == "Enabled"){
		    $signingLink = env("APP_URL")."/view/".$request->document."?signingKey=".$request->signing_key;
		}else{
		    $signingLink = env("APP_URL")."/document/".$request->document."?signingKey=".$request->signing_key;
		}
        $send = Mail::send(
            $request->email, "Signing invitation reminder from ".$user->fname." ".$user->lname,
            array(
                "title" => "Signing invitation reminder.",
                "subtitle" => "Click the link below to respond to the invite.",
                "buttonText" => "Sign Now",
                "buttonLink" => $signingLink,
                "message" => "You have been invited to sign a document by ".$user->fname." ".$user->lname.". Click the link above to respond to the invite.<br><strong>Message:</strong> ".input("message")."<br><br>Cheers!<br>".env("APP_NAME")." Team"
            ),
            "withbutton"
        );
        $activity = '<span class="text-primary">'.escape($user->fname.' '.$user->lname).'</span> sent a signing reminder to <span class="text-primary">'.escape($request->email).'</span>.';
        Signer::keephistory($request->document, $activity, "default");
        if (!$send) { exit(json_encode(responder("error", "Oops!", $send->ErrorInfo))); }
        exit(json_encode(responder("success", "Sent!", "Reminder successfully send.","reload()")));
    }

    /**
     * Decline a signing request 
     * 
     * @return Json
     */
    public function decline() {
    	header('Content-type: application/json');
    	$requestId = input("requestid");
    	$user = Auth::user();
        Database::table("requests")->where("id", $requestId)->update(array("status" => "Declined", "update_time" => date("Y-m-d H-i-s")));
        $request = Database::table("requests")->where("id", $requestId)->first();
        $sender = Database::table("users")->where("id", $request->sender)->first();
        $documentLink = env("APP_URL")."/document/".$request->document;
        $send = Mail::send(
            $sender->email, "Signing invitation declined by ".$user->fname." ".$user->lname,
            array(
                "title" => "Signing invitation declined.",
                "subtitle" => "Click the link below to view document.",
                "buttonText" => "View Document",
                "buttonLink" => $documentLink,
                "message" => $user->fname." ".$user->lname." has declined the signing invitation you had sent. Click the link above to view the document.<br><br>Cheers!<br>".env("APP_NAME")." Team"
            ),
            "withbutton"
        );
        $activity = '<span class="text-primary">'.escape($user->fname.' '.$user->lname).'</span> declined a signing invitation of this document.';
        Signer::keephistory($request->document, $activity, "default");
        $notification = '<span class="text-primary">'.escape($user->fname.' '.$user->lname).'</span> declined a signing invitation of this <a href="'.url("Document@open").$request->document.'">document</a>.';
        Signer::notification($sender->id, $notification, "decline");
        if (!$send) { exit(json_encode(responder("error", "Oops!", $send->ErrorInfo))); }
    	exit(json_encode(responder("success", "Declined!", "Request declined and sender notified.","reload()")));
    }


    private function _SetSekertaris($chain = 'Yes', $emails = [], $document){
        $chainEmailsPlusSek = [];
        if($chain=='Yes'){
            if($emails){
                foreach ($emails as $isi){
                    // cek is set ke sekertaris
                    $sek = DB::table('_sekertaris')
                        ->where('user_parent', '=', $isi)
                        ->first()
                    ;
                    if($sek){
                        $chainEmailsPlusSek[] = $sek->user_sek;
                    }
                    $chainEmailsPlusSek[] = $isi;
                }
            }
            $emails=$chainEmailsPlusSek;
        }else{
            $email_with_sek = [];
            $email_with_no_sek = [];
            if($emails){
                foreach ($emails as $isi){
                    // cek is set ke sekertaris
                    $sek = DB::table('_sekertaris')
                        ->where('user_parent', '=', $isi)
                        ->first()
                    ;
                    if($sek){
                        $email_with_sek[] = $sek->user_sek;
                        $email_with_sek[] = $isi;
                        // call as chain
                        $this->_setChainWithSekretaris($document, $email_with_sek);
                    }else{
                        $email_with_no_sek[] = $isi;
                    }
                }
            }
            $emails=$email_with_no_sek;
        }
        return $emails;
    }

    private function _setChainWithSekretaris($document = [], $emails = []){
        $user = Auth::user();
        $documentKey = $document->document_key;
        $activity = 'Mengajukan tanda tangan ke <span class="text-primary">'.implode(", ", $emails).'</span> oleh <span class="text-primary">'.escape($user->fname.' '.$user->lname).'</span>.';

        $chain = 'Yes';
        if($chain == "Yes"){
            $chainEmails = $emails;
            unset($chainEmails[0]);
            $chainEmails = array_values ( $chainEmails );
            $chainEmails = json_encode($chainEmails);
            $chainPositions[] = "";
            unset($chainPositions[0]);
            $chainPositions = array_values ( $chainPositions );
            $chainPositions = json_encode($chainPositions);
        }
        foreach($emails as $index => $email){
            $signingKey = Str::random(32);
            $signingLink = env("APP_URL")."/document/".$documentKey."?signingKey=".$signingKey;
            $trackerLink = env("APP_URL")."/mailopen?signingKey=".$signingKey;
            $receiverData = Database::table("users")->where("email", $email)->first();
            if (!empty($receiverData)) { $receiver = $receiverData->id; }else{ $receiver = 0; }
            $request = array( "sender_note" => escape(input("message")),  "chain_emails" => $chainEmails, "chain_positions" => $chainPositions, "company" => $user->company, "document" => $documentKey, "signing_key" => $signingKey, "positions" => "", "email" => $email, "sender" => $user->id, "receiver" => $receiver );
            Database::table("requests")->insert($request);
            $send = Mail::send(
                $email, $user->fname." ".$user->lname." has invited you to sign a document",
                array(
                    "title" => "Document Signing invite",
                    "subtitle" => "Click the link below to respond to the invite.",
                    "buttonText" => "Sign Now",
                    "buttonLink" => $signingLink,
                    "message" => "You have been invited to sign a document by ".$user->fname." ".$user->lname.". Click the link above to respond to the invite.<br><strong>Message:</strong> ".input("message")."<br><br>Cheers!<br>".env("APP_NAME")." Team
                    <img src='".$trackerLink."' width='0' height='0'>"
                ),
                "withbutton"
            );
            if (!$send) { exit(json_encode(responder("error", "Oops!", $send->ErrorInfo))); }
        }
        Signer::keephistory($document->document_key, $activity, "default");
        return true;
    }
}
