<?php
namespace Simcify\Controllers;

use Illuminate\Database\Capsule\Manager as DB;
use Ariwa\Attachment as AriwaAttchmnt;
use Simcify\Auth;
use Simcify\Str;
use Ariwa\Uploader as Uploader;

class Attachment{
    public function get(){
        $document_key = input('document_key');
        $attchmnt = DB::table('attachments as a')
            ->selectRaw(" a.*, DATE_FORMAT(a.created_at, '%d/%b/%Y %H:%i') as created_at2, b.fname, b.lname, b.email")
            ->leftJoin('users as b', 'a.created_by', '=', 'b.id')
            ->where('a.document_key', $document_key)
            ->get()
            ;
        return view('ariwa/attachment/attachment', [
            'document_key' => $document_key,
            'attchmnt' => $attchmnt,
        ]);
    }
    public function save(){
        $user = Auth::user();

        $reltive_path = '/uploads/attachment/';
        $storage = config("app.storage")."attachment/";
        $uploader = new Uploader();
        $data = $uploader->upload($_FILES['files'], array(
            'limit' => 10, //Maximum Limit of files. {null, Number}
            'maxSize' => 10, //Maximum Size of files {null, Number(in MB's)}
            'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
            'required' => false, //Minimum one file is required for upload {Boolean}
            'uploadDir' => $storage, //Upload directory {String}
            'title' => array('auto', 32), //New file name {null, String, Array} *please read documentation in README.md
            'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
            'replace' => true, //Replace the file if it already exists  {Boolean}
            'perms' => null, //Uploaded file permisions {null, Number}
            'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
            'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
            'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
            'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
            'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
            'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
        ));

        if($data['isComplete']){
            $info = $data['data'];
            $metas = $info['metas'];
            if($metas){
                foreach ($metas as $isi){
                    DB::table('attachments')
                        ->insert([
                            'document_key' => input('document_key'),
                            'f_name' => $isi['old_name'],
                            'f_file' => $reltive_path.$isi['name'],
                            'f_file_name' => $isi['name'],
                            'f_ext' => $isi['extension'],
                            'f_size' => $isi['size']/1024,
                            'created_by' => $user->id,
                        ]);
                }
            }
            die(json_encode(['stts' => true, 'msg' => 'Upload Success']));
        }

        if($data['hasErrors']){
            $errors = $data['errors'];
            die(json_encode(['stts' => false, 'msg' => $errors]));
        }

    }

    public function del()
    {
        $user = Auth::user();
        $cek = DB::table('attachments')
            ->where('id', input('id'))
            ->where('created_by', $user->id)
            ->first()
            ;
        if(!$cek){
            die(json_encode(['stts' => false, 'msg' => 'File tidak ditemukan']));
        }
        $file = config("app.storage")."attachment/".$cek->f_file_name;

        DB::table('attachments')
            ->where('id', input('id'))
            ->where('created_by', $user->id)
            ->delete()
            ;

        if (file_exists($file)) {
            unlink($file);
        }

        die(json_encode(['stts' => true]));
    }

    public function download($filename) {
        $filename = str_replace('___', '.', $filename);
        $document = DB::table("attachments")->where("f_file_name", $filename)->first();
        if(!$document) return view('errors/filenotfound');
        $file = config("app.storage")."attachment/".$document->f_file_name;
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.$document->f_name.'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        flush();
        readfile($file);
        exit();
    }
}