<?php
namespace Simcify\Controllers;

use Illuminate\Database\Capsule\Manager as DB;
use Pecee\Http\Input\Input;
use Simcify\Auth as Authenticate;
use Simcify\Database;
use Simcify\File;
use Simcify\Sms;
use Ariwa\Fungsional;
use Simcify\Str;
// use Simcify\Mail;

class Auth{

    public function redir() {
        redirect(url("Auth@get")."?secure=true");
    }

    /**
     * Get Auth view
     * 
     * @return \Pecee\Http\Response
     */
    public function get() {
        /*    $send = Mail::send(
            'demo@tokoarlando.com', "Signing invitation declined by ",
            array(
                "title" => "Signing invitation declined.",
                "subtitle" => "Click the link below to view document.",
                "buttonText" => "View Document",
                "buttonLink" => "https://ttd.tokoarlando.com",
                "message" => "Hello Team"
            ),
            "withbutton"
        );
        dd($send);*/
        
        /*$signingKey = Str::random(32);
        dd($signingKey);*/
        /*
        $team = Database::table("users")->where("company", 0)
            ->get(array('*'));
        dd($team);*/
        // cek sms sudah berapa kali hari ini
        /*$tes = Sms::kirim('6281216066929', 'Info Sehat Untuk Anda.');
        dd($tes);*/

        $guest = $signingLink = false;
        if (isset($_COOKIE['guest'])) {
            $guest = true;
            $guestData = unserialize($_COOKIE['guest']);
            $signingLink = url("Guest@open").$guestData[0]."?signingKey=".$guestData[1];
        }
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (!isset($_GET['secure'])) {
            redirect(url("Auth@get")."?secure=true&url=".$actual_link);
        }
        $url = $_GET['url'];
        // return view('login', compact("guest","signingLink"));
        return view('login', ["guest"=>$guest,"signingLink"=>$signingLink, "url"=>$url]);

    }

    /**
     * Sign In a user
     * 
     * @return Json
     */
    public function signin() {
        $signIn = Authenticate::login(
		    input('email'), 
		    input('password'), 
		    array(
		        "rememberme" => true,
		        // "redirect" => url(""),
		        "redirect" => input('url'),
		        "status" => "Active"
		    )
		);

        header('Content-type: application/json');
		exit(json_encode($signIn));
    }

    /**
     * Forgot password - send reset password email
     * 
     * @return Json
     */
    public function forgot() {
        header('Content-type: application/json');
        if(config('app.IS_DEMO')){
            exit(json_encode(responder("success", "Hanya Simulasi!", "Tidak berlaku untuk Demo.","reload()")));
        }
        // cek captcha
        $captcha = $this->_checkCaptcha(input('g-recaptcha-response'));
        if(!$captcha){
            $err = array(
                "status" => "error",
                "title" => "Salah Captcha",
                "message" => "Silahkan ulangi lagi."
            );
            exit(json_encode($err));
        }

        $forgot = Authenticate::forgot(
		    input('email'), 
		    env('APP_URL')."/lupapaswdtoken/[token]"
		);
		exit(json_encode($forgot));
    }

    /**
     * Get reset password view
     * 
     * @return \Pecee\Http\Response
     */
    public function getreset($token) {
        return view('reset', array("token" => $token));
    }

    /**
     * Reset password
     * 
     * @return Json
     */
    public function reset() {
        $reset = Authenticate::reset(
		    input('token'), 
		    input('password')
		);

        header('Content-type: application/json');
		exit(json_encode($reset));
    }

    /**
     * Create an account
     * 
     * @return Json
     */
    public function signup() {
        // cek awal
        // email
        $mail = explode('@',input('email'));
        if(($mail[1]=='mdmedia.co.id' || $mail[1]=='telkom.co.id' || $mail[1]=='metra.co.id')){
        }else{
            Fungsional::msgJsonError("Email salah", "Email yang digunakan harus @mdmedia.co.id, @telkom.co.id atau @metra.co.id");
        }
        if(strlen(input('phone')) < 10 || strlen(input('phone')) > 13){
            Fungsional::msgJsonError("No HP Salah", "Isi No HP antara 10 sampai 13 digit");
        }
        if(!Fungsional::cekIsNumberPhone(input('phone'))){
            Fungsional::msgJsonError("No HP Salah", "Isi No HP dengan benar");
        }
        if(Fungsional::cekNumberPhoneIsExist(input('phone'))){
            Fungsional::msgJsonError("No Already exists.", "No HP Sudah pernah digunakan.");
        }
        if(Fungsional::cekEmailIsExist(input('email'))){
            Fungsional::msgJsonError("Email Already exists.", "Email Sudah pernah digunakan.");
        }

    	if (!empty(input('business'))) {
    		$companyData = array(
    				"name" => input('company'),
    				"email" => input('email')
    			);
            $companyId = DB::table("companies")->insertGetId($companyData);
    	}else{
    		$companyId = 0;
    	}

    	if(!empty(input('sms_id'))){
    	    // cek OTP
            $otp = DB::table('_sms_verification')
                ->where('sms_id',input('sms_id'))
                ->where('sms_code',input('sms_code'))
                ->first();

            if(!empty($otp)){// data ADA
                // cek expired
                $date2 = strtotime('+3 minutes', strtotime($otp->sms_datetime));
                if(date('Y-m-d H:i:s', $date2)<date('Y-m-d H:i:s')){
                    // update data OTP
                    DB::table('_sms_verification')
                        ->where('sms_id',input('sms_id'))
                        ->where('sms_code',input('sms_code'))
                        ->update(array('status'=>'expired'))
                    ;

                    $err = array(
                        "status" => "error",
                        "title" => "OTP.",
                        "message" => "Kode OTP Sudah Kadaluarsa.",
                    );
                    header('Content-type: application/json');
                    exit(json_encode($err));
                }

                // update data OTP
                DB::table('_sms_verification')
                    ->where('sms_id',input('sms_id'))
                    ->where('sms_code',input('sms_code'))
                    ->update(array('status'=>'used', 'sms_datetime_used'=>date('Y-m-d H:i:s')))
                    ;

                /*
                 * ===m
                 */

            }else{
                // ariwa
                $err = array(
                    "status" => "error",
                    "title" => "OTP.",
                    "message" => "OTP Not Valid.",
                );
                header('Content-type: application/json');
                exit(json_encode($err));
            }
        }else{


            // cek captcha
            if(!Fungsional::cekCaptchaIsValid(input('g-recaptcha-response'))){
                Fungsional::msgJsonError("Salah Captcha.", "Silahkan di ulang kembali.");
            }

            // cek sms sudah berapa kali hari ini
            if(Fungsional::cekIsMaksSMSOTPPerHari(input('phone'))){ // 2
                Fungsional::msgJsonError("Max OTP Per Hari.", "Anda sudah melakukan OTP sebanyak <b>".env('SMS_REG_PERDAYMAX')."x</b> hari ini. Silahkan coba kembali esok hari.");
            }
            
            // saat get kode OTP
            $otpkode = Sms::o1_getkodeotp();
            $log_string = array(
                "fname" => input('fname'),
                "lname" => input('lname'),
                "email" => input('email'),
                "phone" => input('phone'),
                'IP'=>$this->_get_client_ip()
            );
            $insert = DB::table('_sms_verification')->insert(array(
                'sms_id'=>$otpkode['sms_id'],
                'sms_code'=>$otpkode['sms_code'],
                'sms_datetime'=>date('Y-m-d H:i:s'),
                'sms_for'=>'registrasi',
                'sms_by'=>input('email'),
                'sms_time'=>'1',
                'sms_no'=>input('phone'),
                'log_string'=>json_encode($log_string)
            ));

            // kirim SMS OTP $vcode
            /*
             *
             */
            $tes = Sms::kirim(input('phone'), 'Kode OTP Signer MDMedia, jangan beritahukan kode ini pada siapapun: '.$otpkode['sms_code']);

            $err = array(
                "status" => "otp",
                'sms_id'=>$otpkode['sms_id']
            );
            header('Content-type: application/json');
            exit(json_encode($err));
        }

        $signup = Authenticate::signup(
		    array(
		        "fname" => input('fname'),
		        "lname" => input('lname'),
		        "email" => input('email'),
                "phone" => input('phone'),
		        "role" => "user",
		        "company" => 0,
		        "password" => Authenticate::password(input('password'))
		    ), 
		    array(
		        "authenticate" => true,
		        "redirect" => url(""),
		        "uniqueEmail" => input('email')
		    ),
            input('avatar')
		);

        header('Content-type: application/json');
		exit(json_encode($signup));
    }

    /**
     * Sign Out a logged in user
     *
     */
    public function signout() {
        Authenticate::deauthenticate();
        redirect(url("Auth@get"));
    }

    public function validasiotp(){
        dd($_POST);
    }

    public function otpulang(){
        // update data OTP
        $data = DB::table('_sms_verification')
            ->where('sms_id',input('sms_id'))
            ->first()
        ;
        DB::table('_sms_verification')
            ->where('sms_id',input('sms_id'))
            ->update(array('status'=>'expired'))
        ;

        // cek sms sudah berapa kali hari ini
        $maxotp = DB::table('_sms_verification')
            ->Where('sms_no', $data->sms_no)
            ->Where('sms_datetime', 'like', '%'.date('Y-m-d').'%')
            ->get();
        if (count($maxotp) >= env('SMS_REG_PERDAYMAX')) {
            $err = array(
                "status" => "error",
                "title" => "Max OTP Per Hari",
                "message" => "Anda sudah melakukan OTP sebanyak <b>".env('SMS_REG_PERDAYMAX').'x</b> hari ini. Silahkan coba kembali esok hari.',
            );
            header('Content-type: application/json');
            exit(json_encode($err));
        }

        // saat get kode OTP
        $otpkode = Sms::o1_getkodeotp();
        if(isset($_POST['indata'])){
            $_POST['indata'][] = array('name'=>'IP', 'value'=>$this->_get_client_ip());
            $indata = $_POST['indata'];
        }else{
            $indata = '-';
        }

        $insert = DB::table('_sms_verification')->insert(array(
            'sms_id'=>$otpkode['sms_id'],
            'sms_code'=>$otpkode['sms_code'],
            'sms_datetime'=>date('Y-m-d H:i:s'),
            'sms_for'=>'reg ulang',
            'sms_by'=>$data->sms_no,
            'sms_time'=>'1',
            'sms_no'=>$data->sms_no,
            'log_string'=>json_encode($indata)
        ));

        // kirim SMS OTP $vcode
        /*
         *
         */
        $tes = Sms::kirim($data->sms_no, 'Kode OTP Signer MDMedia, jangan beritahukan kode ini pada siapapun: '.$otpkode['sms_code']);

        die(json_encode(array('status'=> true,'sms_id'=>$otpkode['sms_id'])));
    }

    private function _checkCaptcha($code_captcha = ''){
        if(isset($code_captcha)){
            $captcha=$code_captcha;
        }
        if(!$captcha){
            return false;
        }
        $secretKey = "6LeI2tMUAAAAAPrUhUFKhdYAsW4RfD_Nn5xr6yNZ";
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) .  '&response=' . urlencode($captcha);
        $response = file_get_contents($url);
        $responseKeys = json_decode($response,true);
        // should return JSON with success as true
        if($responseKeys["success"]) {
            return true;
        } else {
            return false;
        }
    }

    private function _get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }


    /*
     * 2021
     */
    public function loginotp(){
        return view('ariwa/login/otp', []);
    }
    public function cekotppost(){
        $otp = input('otp');
        $is_otp = DB::table('_login_otp')
            ->where('otp_code', $otp)
            ->where('otp_user', session('email'))
            ->whereNull('used_at')
            ->whereRaw('(SYSDATE() <= otp_expired)')
            ->first()
            ;

        if ($is_otp) {
            session('loginotp', 1);
            DB::table('_login_otp')
                ->where('otp_code', $otp)
                ->where('otp_user', session('email'))
                ->update([
                    'used_at' => date('Y-m-d H:i:s')
                ])
                ;
            die(json_encode(['stts'=>true]));
        }else{
            die(json_encode(['stts'=>false, 'msg'=>'OTP Not Valid.']));
        }
    }
}
