<?php
namespace Simcify\Controllers;

use Simcify\Auth;
use Simcify\Database;
use Illuminate\Database\Capsule\Manager as DB;

class Dashboard{
    /**
     * Get dashboard view
     * 
     * @return \Pecee\Http\Response
     */
    public function get() {
        /*
        $certificate = 'file://'.realpath(config("app.storage").'/credentials/tcpdf.crt');
        $pdf = 'file://'.realpath(config("app.storage").'/files/hR5Qe9k7ZB568nHq26D8p7OZnv1M5ckM.pdf');
            $private_key = 'file://'.realpath(config("app.storage").'/credentials/tcpdf.p12');
        $certs = array();
        $pkcs12 = file_get_contents( $private_key );
        openssl_pkcs12_read( $pkcs12, $certs, "ariwa" );


        $content = file_get_contents($pdf);
        $regexp = '#ByteRange\[\s*(\d+) (\d+) (\d+)#'; // subexpressions are used to extract b and c

        $result = [];
        preg_match_all($regexp, $content, $result);

        // $result[2][0] and $result[3][0] are b and c
        if (isset($result[2]) && isset($result[3]) && isset($result[2][0]) && isset($result[3][0]))
        {
            $start = $result[2][0];
            $end = $result[3][0];
            if ($stream = fopen($pdf, 'rb')) {
                $signature = stream_get_contents($stream, $end - $start - 2, $start + 1); // because we need to exclude < and > from start and end

                fclose($stream);
            }
            file_put_contents('signature.pkcs7', hex2bin($signature));
        }
        dd('ok');
        */

        $getversions = DB::table('_versions')
            ->orderBy('version_release_date', 'desc')
            ->select(['*'])
            ->selectRaw("DATE_FORMAT(version_release_date, '%d %b %Y') as version_release_date2")
            ->get()
        ;

    	$user = Auth::user();
    	if ($user->role == "user") {
	        $fileLimit = env("PERSONAL_FILE_LIMIT");
	        $diskLimit = env("PERSONAL_DISK_LIMIT");
	        $diskUsage = Database::table("files")->where("uploaded_by" , $user->id)->sum("size", "size")[0]->size;
	        $fileUsage = Database::table("files")->where("uploaded_by" , $user->id)->count("id", "files")[0]->files;
	        $folders = Database::table("folders")->where("created_by" , $user->id)->count("id", "folders")[0]->folders;
	        // add attachment
            $diskUsage_attcmnt = Database::table("attachments")->where("created_by" , $user->id)->sum("f_size", "f_size")[0]->f_size;
            $fileUsage_attcmnt = Database::table("attachments")->where("created_by" , $user->id)->count("id", "files")[0]->files;
            $diskUsage = $diskUsage+$diskUsage_attcmnt;
            $fileUsage = $fileUsage+$fileUsage_attcmnt;

            // signed vs unsigned
	        $signed = Database::table("files")
                ->where("uploaded_by" , $user->id)
                ->where("deleted_at", "is null")
                ->where("status" , "Signed")
                ->count("id", "total")[0]->total;
	        $unsigned = Database::table("files")
                ->where("uploaded_by" , $user->id)
                ->where("deleted_at", "is null")
                ->where("status" , "Unsigned")
                ->count("id", "total")[0]->total;

	    	// file types stats
	    	$myPdf = Database::table("files")->where(array("extension" => "pdf", "uploaded_by" => $user->id), "=")
					    	    ->count("id", "files")[0]->files;
	    	$myWord = Database::table("files")->where(array("extension" => "doc", "uploaded_by" => $user->id), "=")
	    						->orWhere("extension","docx")->Where("uploaded_by", $user->id)->count("id", "files")[0]->files;
	    	$myExcel = Database::table("files")->where(array("extension" => "xls", "uploaded_by" => $user->id), "=")
	    						->orWhere("extension","xlsx")->Where("uploaded_by", $user->id)->count("id", "files")[0]->files;
	    	$myPpt = Database::table("files")->where(array("extension" => "ppt", "uploaded_by" => $user->id), "=")
	    						->orWhere("extension","pptx")->Where("uploaded_by", $user->id)->count("id", "files")[0]->files;

	    	// pending signing requests
	    	$pendingRequests = Database::table("requests")->where("sender" , $user->id)->where("status" , "Pending")->count("id", "total")[0]->total;
    	}else{
	        $fileLimit = env("BUSINESS_FILE_LIMIT");
	        $diskLimit = env("BUSINESS_DISK_LIMIT");
	        $diskUsage = Database::table("files")->where("company" , $user->company)->sum("size", "size")[0]->size;
	        $fileUsage = Database::table("files")->where("company" , $user->company)->count("id", "files")[0]->files;
	        $folders = Database::table("folders")->where("company" , $user->company)->count("id", "folders")[0]->folders;
            // add attachment
            $diskUsage_attcmnt = Database::table("attachments")->sum("f_size", "f_size")[0]->f_size;
            $fileUsage_attcmnt = Database::table("attachments")->count("id", "files")[0]->files;
            $diskUsage = $diskUsage+$diskUsage_attcmnt;
            $fileUsage = $fileUsage+$fileUsage_attcmnt;

	        // signed vs unsigned
	        $signed = Database::table("files")->where("company" , $user->company)->where("status" , "Signed")->count("id", "total")[0]->total;
	        $unsigned = Database::table("files")->where("company" , $user->company)->where("status" , "Unsigned")->count("id", "total")[0]->total;

	    	// file types stats
	    	$myPdf = Database::table("files")->where(array("extension" => "pdf", "company" => $user->company), "=")
					    	    ->count("id", "files")[0]->files;
	    	$myWord = Database::table("files")->where(array("extension" => "doc", "uploaded_by" => $user->id), "=")
	    						->orWhere("extension","docx")->Where("company", $user->company)->count("id", "files")[0]->files;
	    	$myExcel = Database::table("files")->where(array("extension" => "xls", "uploaded_by" => $user->id), "=")
	    						->orWhere("extension","xlsx")->Where("company", $user->company)->count("id", "files")[0]->files;
	    	$myPpt = Database::table("files")->where(array("extension" => "ppt", "uploaded_by" => $user->id), "=")
	    						->orWhere("extension","pptx")->Where("company", $user->company)->count("id", "files")[0]->files;

	    	// pending signing requests 
	    	$pendingRequests = Database::table("requests")->where("company" , $user->company)->where("status" , "Pending")->count("id", "total")[0]->total;
    	}

    	if ($user->role == "superadmin") {
	    	// system usage stats
	    	$systemDisk = Database::table("files")->sum("size", "size")[0]->size;
	    	$systemFiles = Database::table("files")->count("id", "files")[0]->files;
	    	$systemUsers = Database::table("users")->count("id", "users")[0]->users;
            // add attachment
            $diskUsage_attcmnt = Database::table("attachments")->sum("f_size", "f_size")[0]->f_size;
            $fileUsage_attcmnt = Database::table("attachments")->count("id", "files")[0]->files;
            $systemDisk = $systemDisk+$diskUsage_attcmnt;
            $systemFiles = $systemFiles+$fileUsage_attcmnt;
	    	// account type stats
	    	$businessAccounts = Database::table("companies")->where("id",">", 0)->count("id", "companies")[0]->companies;
	    	$personalAccounts = Database::table("users")->where("role", "user")->where("company", "0")->count("id", "users")[0]->users;

	    	// file types stats
	    	$totalPdf = Database::table("files")->where("extension", "pdf")->count("id", "files")[0]->files;
	    	$totalWord = Database::table("files")->where("extension", "doc")->orWhere("extension", "docx")->count("id", "files")[0]->files;
	    	$totalExcel = Database::table("files")->where("extension", "xls")->orWhere("extension", "xlsx")->count("id", "files")[0]->files;
	    	$totalPpt = Database::table("files")->where("extension", "ppt")->orWhere("extension", "pptx")->count("id", "files")[0]->files;

	        return view('dashboard', compact("user","fileUsage","diskUsage","diskLimit","fileLimit","folders","pendingRequests","signed","unsigned","myPdf","myWord","myExcel","myPpt","systemDisk","systemFiles","systemUsers","businessAccounts","personalAccounts","totalPdf","totalWord","totalExcel","totalPpt","getversions"));
    	}else{
	        return view('dashboard', compact("user","fileUsage","diskUsage","diskLimit","fileLimit","folders","pendingRequests","signed","unsigned","myPdf","myWord","myExcel","myPpt","getversions"));
    	}
    }

    public function versions() {
        $getversions = DB::table('_versions')
            ->orderBy('version_release_date', 'desc')
            ->select(['*'])
            ->selectRaw("DATE_FORMAT(version_release_date, '%d %b %Y') as version_release_date2")
            ->get()
            ;

        return view('ariwa/version', compact('getversions'));
    }
}
