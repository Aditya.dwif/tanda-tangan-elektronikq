<?php
namespace Simcify;

use AfricasTalking\SDK\AfricasTalking;
use Twilio\Rest\Client;
use Simcify\Database;

class Sms {
    /*Karet*/
    private $username = 'hcmdm'; //KRT
    private $password = 'xxxxxx'; //KRT
    private $sender = 'xxxxxx';
    private $endpoint = 'https://xxxxxxxxxxx.xx'; //krt
    private $data;

    /**
     * Send SMS With Africa's Talking
     * 
     * @param   string $phone number
     * @param   string $message
     * @return  array
     */
    public static function africastalking($phoneNumber,$message) {
        $username = env('AFRICASTALKING_USERNAME'); 
        $apiKey   = env('AFRICASTALKING_KEY'); 
        $from   = env('AFRICASTALKING_SENDERID');
        $AT       = new AfricasTalking($username, $apiKey);
        
        // Get one of the services
        $sms      = $AT->sms();
        
        if(empty($from)){
           // Use the service
            $response   = $sms->send([
                'to'      => $phoneNumber,
                'message' => $message
            ]); 
        }else{
            // Use the service
            $response   = $sms->send([
                'to'      => $phoneNumber,
                'message' => $message,
                'from' => $from
            ]);
        }

        if ($response["status"] == "success") {
            return true;
        }else{
            return false;
        }
    }

    /**
     * Send SMS With Twilio
     * 
     * @param   string $phone number
     * @param   string $message
     * @return  array
     */
    public static function twilio($phoneNumber,$message) {

        $client = new Client(env('TWILIO_SID'), env('TWILIO_AUTHTOKEN'));
        try{ 
            $response = $client->account->messages->create(
                $phoneNumber,
                array(
                    'from' => env('TWILIO_PHONENUMBER'), 
                    'body' => $message
                )
            );
          }catch(\Exception $e){  
            return false;
          }

        if ($response->status == "sent" || $response->status == "queued") {
            return true;
        }else{
            return false;
        }
    }

    /*
     * ARIWA
     */
    public static function o1_getkodeotp(){
    // saat get kode OTP
        $vcode = rand(100000, 999999);
        $sms_id = 'otp_'.md5(uniqid());
        return array('sms_code'=>$vcode, 'sms_id'=>$sms_id);
    }

    public static function kirim($msisdn, $message){
        $ini = new self();
        $headers = [
            "Authorization:Basic ". base64_encode($ini->username . ":" . $ini->password),
            "Content-Type:application/x-www-form-urlencoded"
        ];
        $vars = [
            "sender" => $ini->sender,
            "msisdn" => $msisdn,
            "message" => $message
        ];
        $data = http_build_query($vars);
        //return $data;
        //return $ini->endpoint . '|' . $vars;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $ini->endpoint);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "UTF-8");
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        $ini->data = curl_exec($ch);
        curl_close($ch);
        $status = explode('|', $ini->data);
        return ($status[0] == 'SENT') ? true : false;
    }

    public static function kirimtes($msisdn, $message){
        $ini = new self();
        $headers = [
            "Authorization:Basic ". base64_encode($ini->username . ":" . $ini->password),
            "Content-Type:application/x-www-form-urlencoded"
        ];
        $vars = [
            "sender" => $ini->sender,
            "msisdn" => $msisdn,
            "message" => $message
        ];
        $data = http_build_query($vars);
        //return $data;
        //return $ini->endpoint . '|' . $vars;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $ini->endpoint);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "UTF-8");
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        $ini->data = curl_exec($ch);
        curl_close($ch);
        $status = explode('|', $ini->data);
        return ($status[0] == 'SENT') ? true : false;
    }
}

