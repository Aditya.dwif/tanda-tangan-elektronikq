<?php
namespace Ariwa;

use Illuminate\Database\Capsule\Manager as DB;

class Fungsional {
    public function __construct()
    {

    }

    public static function msgJsonError($title = '', $msg = ''){
        $err = array(
            "status" => "error",
            "title" => $title,
            "message" => $msg
        );
        header('Content-type: application/json');
        exit(json_encode($err));
    }

    public static function cekIsNumberPhone($phone = ''){
        if( strpos( substr($phone, 0,3) , '8' ) !== false) { // JIKA ADA ANGKA 8
            return true;
        }
        return false;
    }

    public static function cekNumberPhoneIsExist($phone = ''){
        $number_after_8 = substr($phone, strpos($phone, "8") + 1);
        $get_number_is_exist = DB::table('users')->whereRaw("SUBSTR(phone FROM (INSTR(phone, '8') + 1)) = '$number_after_8'")
            ->first()
        ;
        if($get_number_is_exist) return true;
        return false;
    }

    public static function cekEmailIsExist($email = ''){
        $mail = DB::table(config('auth.table'))
            ->where(config('auth.emailColumn'),$email)
            ->first();
        if($mail){
            return true;
        }
        return false;
    }

    public static function cekCaptchaIsValid($code_captcha = ''){
        if(isset($code_captcha)){
            $captcha=$code_captcha;
        }
        if(!$captcha){
            return false;
        }
        $secretKey = "6LeI2tMUAAAAAPrUhUFKhdYAsW4RfD_Nn5xr6yNZ";
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) .  '&response=' . urlencode($captcha);
        $response = file_get_contents($url);
        $responseKeys = json_decode($response,true);
        // should return JSON with success as true
        if($responseKeys["success"]) {
            return true;
        } else {
            return false;
        }
    }

    public static function cekIsMaksSMSOTPPerHari($phone = ''){
        $number_after_8 = substr($phone, strpos($phone, "8") + 1);
        $maxotp = DB::table('_sms_verification')
            ->whereRaw("SUBSTR(sms_no FROM (INSTR(sms_no, '8') + 1)) = '$number_after_8'")
            ->Where('sms_datetime', 'like', '%'.date('Y-m-d').'%')
            ->get();
        if(count($maxotp) >= env('SMS_REG_PERDAYMAX')){
            return true;
        }
        return false;
    }

    public static function getIconDocument($file_type = '', $width = '25'){
        switch ($file_type){
            case 'doc':
                return '<img src="/assets/images/formats/doc.png" class="img-responsive" style="float: left;margin-right: 5px;" width="'.$width.'">';
                ;
            case 'docx':
                return '<img src="/assets/images/formats/docx.png" class="img-responsive" style="float: left;margin-right: 5px;" width="'.$width.'">';
                ;
            case 'xls':
                return '<img src="/assets/images/formats/xls.png" class="img-responsive" style="float: left;margin-right: 5px;" width="'.$width.'">';
                ;
            case 'xlsx':
                return '<img src="/assets/images/formats/xlsx.png" class="img-responsive" style="float: left;margin-right: 5px;" width="'.$width.'">';
                ;
            case 'ppt':
                return '<img src="/assets/images/formats/ppt.png" class="img-responsive" style="float: left;margin-right: 5px;" width="'.$width.'">';
                ;
            case 'pptx':
                return '<img src="/assets/images/formats/pptx.png" class="img-responsive" style="float: left;margin-right: 5px;" width="'.$width.'">';
                ;
            case 'zip':
                return '<img src="/assets/images/formats/zip.png" class="img-responsive" style="float: left;margin-right: 5px;" width="'.$width.'">';
                ;
            case 'pdf':
                return '<img src="/assets/images/formats/pdf.png" class="img-responsive" style="float: left;margin-right: 5px;" width="'.$width.'">';
                ;
            default:
                return '<img src="/assets/images/formats/unknown.png" class="img-responsive" style="float: left;margin-right: 5px;" width="'.$width.'">';
                ;
        }
    }
}

