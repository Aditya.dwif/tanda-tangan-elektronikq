<?php
namespace Ariwa;

require_once __DIR__.'/../TCPDF/tcpdf.php';
require_once __DIR__.'/../TCPDF/tcpdi.php';

use Simcify\Str;
use TCPDF;
use TCPDI;
use Simcify\File;
use Simcify\Auth;
use Simcify\Database;
use \CloudConvert\Api;
use Simcify\Signer;
use Illuminate\Database\Capsule\Manager as DB;

class PDF extends TCPDI {
    var $_tplIdx;
    var $numPages;

    function Header() {}

    function Footer() {}

}

class SK2020 {
    public function __construct()
    {

    }

    public function setTTD($pars){
        $user = Auth::user();
        $userName = $user->fname.' '.$user->lname;
        $signature = config("app.storage")."signatures/".$user->signature;
        $paraf = config("app.storage")."paraf/".$user->paraf;

        $document = Database::table("files")->where("document_key", $pars->document_key)->first();
        $pdf = new PDF(null, 'px');
        $pdf->SetAutoPageBreak(FALSE, PDF_MARGIN_BOTTOM);
        $inputPath = config("app.storage")."files/".$document->filename;
        $outputName = Str::random(32).".pdf";
        $outputPath = config("app.storage")."/files/". $outputName;
        $pdf->numPages = $pdf->setSourceFile($inputPath); // error

        try {
            $pdf->_tplIdx = $pdf->importPage(1); // halaman berapa
        }
        catch(\Exception $e) {
            return false;
        }

        $size = $pdf->getTemplateSize($pdf->_tplIdx);
        $pdf->AddPage(self::orientation($size['w'], $size['h']), array($size['w'], $size['h'], 'Rotate'=>0), true);
        $pdf->useTemplate($pdf->_tplIdx);

        $pdf->Image($signature, 200, 200, 100, 100, '', '', '', false);



    }

    public function loopSign(){
        $user = Auth::user();
        $requestsData = DB::table("requests as r")
            ->whereRaw("(r.receiver = '".$user->id."' or r.email='".$user->email."')")
            ->where("r.status", '=', 'Pending')
            ->where("f.folder", '=', self::getFolderId())
            ->leftJoin('files as f', 'f.document_key', '=', 'r.document')
            ->leftJoin('users as u', 'u.id', '=', 'r.sender')
            ->select(['r.*','f.*', 'u.email as sender2'])
            ->selectRaw("DATE_FORMAT(r.send_time, '%d/%b/%Y %H:%i') as send_time2")
            ->orderBy("r.send_time", 'desc')
            ->get();

        $page = 2;

        if($requestsData->count()){
            $total = $requestsData->count();
            $i = 0;

            $error = [];
            foreach ($requestsData as $isi){
                $i += 1;
                $percent = intval($i/$total * 100);
                $arr_content['percent'] = $percent;
                $arr_content['message'] = "<b>".$i."</b> dari <b>".$total."</b> Document telah di proses.";
                file_put_contents("uploads/tmp/hist20200622.txt", json_encode($arr_content));

                // get pdf file properties
                $pdf = new PDF(null, 'px');
                $pdf->SetAutoPageBreak(FALSE, PDF_MARGIN_BOTTOM);
                $inputPath = config("app.storage")."files/".$isi->filename;
                $pdf->setSourceFile($inputPath); // error
                $width_pdf = $pdf->getPageWidth();

                // ttd pak SY
                /*$actions[] = [
                    "type" => "signature",
                    "page" => $page,
                    "xPos" => 278,
                    "yPos" => 700,
                    "width" => 100,
                    "height" => 35,
                    "image" => "/uploads/signatures/".$user->signature,
                    "text" => "",
                    "fontsize" => "",
                    "group" => "field",
                ];*/

                // paraf pak Eddy
                $actions[] = [
                    "type" => "signature",
                    "page" => $page,
                    "xPos" => 334,
                    "yPos" => 720,
                    "width" => 90,
                    "height" => 35,
                    "image" => "/uploads/paraf/".$user->paraf,
                    "text" => "",
                    "fontsize" => "",
                    "group" => "field",
                ];

                $sign = Signer::sign($isi->document_key, $actions, $width_pdf, $isi->signing_key, false, true);
                if (!$sign) {
                    $error[] = $isi->name;
                }
                sleep(1);
            }
            if($error){
                return $error;
            }
        }
    }

    public static function getFolderId(){
        return '2';
    }
}

