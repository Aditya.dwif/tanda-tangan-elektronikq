<?php
namespace Ariwa;

use Illuminate\Database\Capsule\Manager as DB;
use Simcify\Auth;
use Simcify\Database;

class CekHakAksesDokumen
{
    public function __construct()
    {

    }

    public static function cekdokumen($document_key = ''){
        $user = Auth::user();

        // cek apakah pemilik langsung
        $cek1 = DB::table('files')
            ->where('document_key', $document_key)
            ->where('uploaded_by', $user->id)
            ->first()
        ;
        if($cek1) return true;

        // cek ke request
        $cek = DB::table('requests')
            ->where('document', $document_key)
            ->whereRaw("(sender=".$user->id." or receiver=".$user->id.")")
            ->first()
        ;
        if($cek)
            return true;
        else
            return false;
    }


    // apakah benar2 owner dok nya
    public static function cekdokumen_by_id($id = 0){
        $user = Auth::user();

        // cek apakah pemilik langsung
        $cek1 = DB::table('files')
            ->where('id', $id)
            ->where('uploaded_by', $user->id)
            ->first()
        ;
        if($cek1) return true;
        else
            return false;
    }

    // apakah dokumen ini belum di tanda tangan oleh user lain ?
    // untuk cek sebelum delete file permanent
    public static function isSignedOtherUser($document_key = ''){
        // cek apakah pemilik langsung
        $cek1 = DB::table('requests')
            ->where('document', $document_key)
            ->where('status', '=', 'Signed')
            ->first()
        ;
        if($cek1){
            return true;
        }
        else{
            // cek jika dokumen sudah pernah di tanda tangan
            $cek2 = DB::table('history')
                ->where('file', $document_key)
                ->where('type', '=', 'success')
                ->first()
            ;
            if($cek2){
                return true;
            }else{
                return false;
            }
        }

    }
}