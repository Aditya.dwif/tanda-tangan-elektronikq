<?php
namespace Ariwa;

use Illuminate\Database\Capsule\Manager as DB;
use Simcify\Auth;
use Simcify\Database;

class Attachment
{
    public function __construct()
    {

    }

    public static function qDasar(){

        $fileName = Str::random(32).".".$options["extension"];
        $outputFile = $storage."/".$fileName;

        if ($options['source'] == "form") {
            if(!move_uploaded_file($file["tmp_name"], $outputFile)){
                return array(
                    "status" => "error",
                    "title" => "Upload Failed",
                    "message" => "Something went wront uploading file."
                );
            }
        }
    }

    public static function cekStrageUser(){
        $user = Auth::user();
        $fileUsage = Database::table("files")->where("uploaded_by" , $user->id)->count("id", "files")[0]->files;
        $diskUsage = Database::table("files")->where("uploaded_by" , $user->id)->sum("size", "size")[0]->size / 1000;
        // check file usage limits
        if ($fileUsage > env("PERSONAL_FILE_LIMIT")) {
            return responder("error", "Limit Exceeded!", "Maaf, Kapasitas Anda sudah melebihi ".env("PERSONAL_FILE_LIMIT")." files.");
        }
        // check disk usage limits
        if ($diskUsage > env("PERSONAL_DISK_LIMIT")) {
            return responder("error", "Limit Exceeded!", "Maaf, Kapasitas Anda sudah melebihi ".env("PERSONAL_DISK_LIMIT")." MBs.");
        }
    }
}