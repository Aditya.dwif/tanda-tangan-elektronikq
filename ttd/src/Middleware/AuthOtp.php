<?php
namespace Simcify\Middleware;

use Pecee\Http\Middleware\IMiddleware;
use Pecee\Http\Request;
use Simcify\Database;
use Simcify\Auth;
use Simcify\Signer;

class AuthOtp implements IMiddleware {

    /**
     * Redirect the user if they are unautenticated
     *
     * @param   \Pecee\Http\Request $request
     * @return  \Pecee\Http]Request
     */
    public function handle(Request $request) {
        if(!config('app.IS_DEMO')) { // jika tidak demo
            if(config('app.OTP_AKTIF')) {
                if (!session()->has('loginotp')) {
                    $request->setRewriteUrl(url('Auth@loginotp'));
                }
            }
        }
        return $request;

    }

}
