<?php
include_once 'vendor/autoload.php';

ini_set('display_errors', 'Off');
// ini_set('display_errors', 'On');
// error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_DEPRECATED);

//putenv('APP_URL=http://'.$_SERVER['SERVER_NAME']);

use Illuminate\Database\Capsule\Manager as Capsule;
use Simcify\Application;

$capsule = new Capsule;
$app = new Application();

$capsule->addConnection([
    "driver" => "mysql",
    'host'      => 'localhost',
    'database'  => 'ttd_pt_panjimas',
    'username'  => 'root',
    'password'  => '',
]);
//Make this Capsule instance available globally.
$capsule->setAsGlobal();
// Setup the Eloquent ORM.
$capsule->bootEloquent();
$app->route();
