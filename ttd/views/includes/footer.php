
    <footer>
        <p class="text-right">&copy; <?=date("Y")?> <?=env("APP_NAME")?> | All Rights Reserved. Ver {{ env("APP_VERSION") }}</p>
    </footer>

    <script type="text/javascript">
    	var countNotificationsUrl = '<?=url("Notification@count");?>';
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-7TS6XPK6J5"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-7TS6XPK6J5');
    </script>