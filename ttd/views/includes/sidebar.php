<style>
    .left-bar a {
        margin: 0 0 10px 10px !important;
        color: #ddd;
    }
</style>

<div class="left-bar" style="overflow-y: auto;">
    <div class="slimscroll-menu">
        <li><a href="<?=url("");?>">
                <label class="menu-icon"><i class="ion-ios-speedometer"></i> </label><span class="text">Dashboard</span>
            </a></li>
        <li><a href="<?=url("Notification@get");?>" class="notification-holder">
                <label class="menu-icon"><i class="ion-ios-bell"></i> </label><span class="text">Notifikasi</span>
            </a></li>
        <li><a href="#">
                <label class="menu-icon"><i class="ion-document-text"></i> </label><span class="text">Documents</span>
            </a>
            <ul>
                <li><a href="<?=url("Document@get");?>">
                        @if ( $user->role == "superadmin" )
                        <label class="menu-icon"><i class="ion-document"></i> </label><span class="text">My Documents</span>
                        @else
                        <label class="menu-icon"><i class="ion-document"></i> </label><span class="text">My Documents</span>
                        @endif
                    </a></li>
                <li><a href="<?=url("Document@getneedmysign");?>">
                        <label class="menu-icon"><i class="ion-alert"></i> </label><span class="text">Need My Sign</span>
                    </a></li>
                <li><a href="<?=url("Document@getsigned");?>">
                        <label class="menu-icon"><i class="ion-checkmark"></i> </label><span class="text">Signed</span>
                    </a></li>
            </ul>
        </li>
        <li><a href="<?=url("Request@get");?>">
                <label class="menu-icon"><i class="ion-android-checkmark-circle"></i> </label><span class="text">Status Pengajuan</span>
            </a></li>
        <li><a href="<?=url("Template@get");?>">
                <label class="menu-icon"><i class="ion-document"></i> </label><span class="text">Templates</span>
            </a></li>

        @if ( $user->role == "superadmin" )
        @if ( env('SHOW_SAAS') == "Enabled" )
        <li><a href="<?=url("Company@get");?>">
                <label class="menu-icon"><i class="ion-ios-flower"></i> </label><span class="text">Companies</span>
            </a></li>
        @endif
        <li><a href="<?=url("User@get");?>">
                <label class="menu-icon"><i class="ion-person"></i> </label><span class="text">Users</span>
            </a></li>

        @endif
        <li><a href="<?=url("Settings@get");?>">
                <label class="menu-icon"><i class="ion-gear-a"></i> </label><span class="text">Settings</span>
            </a></li>

        @if ( $user->id == "0" )
        <li><a href="<?=url("Document@dir");?>">
                <label class="menu-icon"><i class="ion-ios-flower"></i> </label><span class="text">SK 2020</span>
            </a></li>
        @endif
    </div>
</div>
