    <style type="text/css">
        .img-header, .thumbnail > img, .thumbnail a > img, .carousel-inner > .item > img, .carousel-inner > .item > a > img {
            display: block;
            max-width: 100%;
            height: 45px;
            margin-top: 0;
        }
    </style>

    <header>
        <!-- Hambager -->
        <div class="humbager">
            <i class="ion-navicon-round"></i>
        </div>
        <!-- logo -->
        <div class="logo">
            <a href="<?=url("");?>">
                <img src="<?=url("");?>uploads/app/{{ env('APP_LOGO'); }}" class="img-header">
            </a>
        </div>

        <!-- top right -->
        <ul class="nav header-links pull-right">
            <li class="notify  hidden-xs">
                <a href="{{ url('Notification@get') }}" class="notification-holder">
                    <span class="notifications">
                        <i class="notifications-count ion-ios-bell"></i>
                    </span>
                </a>
            </li>
            <li class="profile">
                <div class="dropdown">
                    <span class="dropdown-toggle" data-toggle="dropdown">
                        <span class="profile-name"> <span class="hidden-xs"> {{ $user->fname }} </span> <i class="ion-ios-arrow-down"></i> </span>
                        <span class="avatar">
                            @if( !empty($user->avatar) )
                            <img src="<?=url("");?>uploads/avatar/{{ $user->avatar }}" class="user-avatar">
                            @else
                            <img src="<?=url("");?>assets/images/avatar.png" class="user-avatar">
                            @endif
                        </span>
                    </span>
                    <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="menu1">
                        <li role="presentation"><a role="menuitem" href="<?=url("Settings@get");?>">
                                <i class="ion-ios-person-outline"></i> Profile</a></li>
                        <li role="presentation" class="divider"></li>
                        <li role="presentation"><a role="menuitem" href="<?=url("Settings@get");?>">
                                <i class="ion-ios-gear-outline"></i> Settings</a></li>
                        <li role="presentation" class="divider"></li>
                        @if ( env('CERTIFICATE_DOWNLOAD') == "Enabled" )
                        <li role="presentation"><a role="menuitem" href="<?=url("");?>uploads/downloads/credentials.zip" download>
                                <i class="ion-ios-locked-outline"></i> P12 Cert</a></li>
                        <li role="presentation" class="divider"></li>
                        @endif
                        <li role="presentation"><a role="menuitem" href="<?=url("Auth@signout");?>">
                                <i class="ion-ios-arrow-right"></i> Logout</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </header>