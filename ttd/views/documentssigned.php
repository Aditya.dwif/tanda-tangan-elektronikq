<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ config('app.metadata')['title_desc'] }}">
    <meta name="author" content="{{ config('app.metadata')['title_desc'] }}">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=url("");?>uploads/app/{{ env('APP_ICON'); }}">
    <title>Dokumen | {{ config('app.metadata')['title'] }}</title>
    <!-- Ion icons -->
    <link href="<?=url("");?>assets/fonts/ionicons/css/ionicons.css" rel="stylesheet">
    <link href="<?=url("");?>assets/ariwa/font-awesome/css/all.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?=url("");?>assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/select2/css/select2.min.css" rel="stylesheet">
    <link href="<?=url("");?>assets/css/simcify.min.css" rel="stylesheet">
    <!-- Signer CSS -->
    <link href="<?=url("");?>assets/css/style.css" rel="stylesheet">

    <script src="<?=url("");?>assets/js/jquery-3.2.1.min.js"></script>
    <style>
        .dataTables_filter {
            float: left !important;
        }
    </style>
</head>

<body>

    <!-- header start -->
    {{ view("includes/header", $data); }}

    <!-- sidebar -->
    {{ view("includes/sidebar", $data); }}

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <h3>My Sign</h3>
                <div class="breadcrumbs text-muted" style="margin-bottom: 10px;">Dokumen-dokumen User lain yang telah Anda tanda tangan.</div>
                <div id="document_is_approve"></div>
            </div>
        </div>

    </div>

    <!-- footer -->
    {{ view("includes/footer"); }}

    <script src="<?=url("");?>assets/libs/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        var baseUrl = '<?=url("");?>';
    </script>

    <script src="<?=url("");?>assets/ariwa/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="<?=url("");?>assets/ariwa/DataTables/media/js/dataTables.bootstrap.min.js"></script>
    <script src="<?=url("");?>assets/js/ariwa/documents.js"></script>

    <script src="<?=url("");?>assets/js/simcify.min.js"></script>
    <script src="<?=url("");?>assets/js/app.js"></script>
</body>

</html>
