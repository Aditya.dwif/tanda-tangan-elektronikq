<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ config('app.metadata')['title_desc'] }}">
    <meta name="author" content="{{ config('app.metadata')['title_desc'] }}">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=url("");?>uploads/app/{{ env('APP_ICON'); }}">
    <title>Dokumen | {{ config('app.metadata')['title'] }}</title>
    <!-- Ion icons -->
    <link href="<?=url("");?>assets/fonts/ionicons/css/ionicons.css" rel="stylesheet">
    <link href="<?=url("");?>assets/ariwa/font-awesome/css/all.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?=url("");?>assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/select2/css/select2.min.css" rel="stylesheet">
    <link href="<?=url("");?>assets/css/simcify.min.css" rel="stylesheet">
    <!-- Signer CSS -->
    <link href="<?=url("");?>assets/css/style.css" rel="stylesheet">
    <script src="<?=url("");?>assets/js/jquery-3.2.1.min.js"></script>
</head>

<body>

<style>
    #laoding_utama {
        position: fixed;
        width: 100%;
        height:100%;
        left: 0;
        top: 0;
        display: none;
        align-items: center;
        background-color: #000;
        z-index: 999999999999999;
        opacity: 0.7;
    }
    .loading-icon{
        position:absolute;
        border-top:2px solid #fff;
        border-right:2px solid #fff;
        border-bottom:2px solid #fff;
        border-left:2px solid #767676;border-radius:25px;width:25px;height:25px;margin:0 auto;position:absolute;left:50%;margin-left:-20px;top:50%;margin-top:-20px;z-index:4;-webkit-animation:spin 1s linear infinite;-moz-animation:spin 1s linear infinite;animation:spin 1s linear infinite;}
    @-moz-keyframes spin { 100% { -moz-transform: rotate(360deg); } }
    @-webkit-keyframes spin { 100% { -webkit-transform: rotate(360deg); } }
    @keyframes spin { 100% { -webkit-transform: rotate(360deg); transform:rotate(360deg); } }

</style>
<div id="laoding_utama">
    <div class="loading-icon"></div>
    <div style="position: absolute;width: 100%;top: 55%;left: 20%;">
        <style>
            #progress {
                width: 60%;
                border: 1px solid #aaa;
                height: 20px;
            }
            #progress .bar {
                background-color: #ccc;
                height: 20px;
            }
            #message {
                color: #fff;
            }
        </style>
        <div id="progress"></div>
        <div id="message"></div>
    </div>
</div>
<!-- header start -->
{{ view("includes/header", $data); }}

<!-- sidebar -->
{{ view("includes/sidebar", $data); }}

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div><button class="btn btn-primary" onclick="bulksign(event)">Bulk Signing</button></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <h3>Need Sign</h3>
            <div style="margin-top: -10px;font-size: 10px;">Dokumen-dokumen yang perlu Anda tanda tangan.</div>
            <div id="document_need_apprv_dir"></div>
        </div>
        <div class="col-md-6">
            <h3>My Sign</h3>
            <div style="margin-top: -10px;font-size: 10px;">Dokumen-dokumen yang telah Anda tanda tangan.</div>
            <div id="document_is_approve_dir"></div>
        </div>
    </div>

</div>

<!-- footer -->
{{ view("includes/footer"); }}


<!-- scripts -->
<script src="<?=url("");?>assets/libs/jquery-ui/jquery-ui.min.js"></script>
<script src="<?=url("");?>assets/libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=url("");?>assets/js//jquery.slimscroll.min.js"></script>
<script type="text/javascript">
    var baseUrl = '<?=url("");?>';
</script>

<script src="<?=url("");?>assets/ariwa/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?=url("");?>assets/ariwa/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script>
    var a_loader = '<div class="loader-box"><div class="circle-loader"></div></div>';
    var timer;

    $(document).ready(function($) {
        getDataNeedApproval();
        getDataIsApprove();
    });

    function getDataNeedApproval() {
        $('#document_need_apprv_dir').load(baseUrl+'documents/sk2020needapproval', function() {
            $('#document_need_appr_table').DataTable({
                // "fnDrawCallback": function ( oSettings ) {
                //     $(oSettings.nTHead).hide();
                // },
                "lengthChange": false,
                "pageLength": 500,
                // "bInfo": false,
                "scrollY": "250px",
                "scrollCollapse": true,
                "paging":   false,
                // "dom": '<"top"f><"clear"><"bottom"p>',
                // "scrollY": "20px",
                "fnInitComplete" : function(oSettings, json) {
                    // Find the wrapper and hide all thead
                    $('.dataTables_scrollHead').height(1);
                }
            });
            $('#laoding_utama').hide();
        });

    }
    function getDataIsApprove() {
        $('#document_is_approve_dir').load(baseUrl+'documents/sk2020isapprove', function() {
            $('#doc_my_approval_table').DataTable({
                "fnDrawCallback": function ( oSettings ) {
                    $(oSettings.nTHead).hide();
                },
                "lengthChange": false,
                "pageLength": 500,
                // "bInfo": false,
                "scrollY": "250px",
                "scrollCollapse": true,
                "paging":   false,
                // "dom": '<"top"f><"clear"><"bottom"p>',
                // "scrollY": "20px",
                "fnInitComplete" : function(oSettings, json) {
                    // Find the wrapper and hide all thead
                    $('.dataTables_scrollHead').height(1);
                }
            });
            $('#laoding_utama').hide();
        });
    }

    function bulksign(e) {
        if(confirm("Are you sure ?")){
            timer = window.setInterval(getProgress, 1000);
            // setMsgLoaing(e);
            $('#laoding_utama').show();
            $.ajax({
                url:baseUrl+'documents/bulkttdsk2020get',
                dataType:"json",
                // data:{ par: '', 'csrf-token': '<?=csrf_token();?>' },
                // type:'POST',
            });
        }
    }

    function getProgress() {

        // $("#progress").html('<div class="bar" style="width:' + 50 + '%;background-color: #49CB41 !important;"></div>');
        // $("#message").html('<b>10 data dari 180 data</b>');
        // return false;
        /*var strt = window.setInterval(function(){
            $.get(baseUrl+'documents/bulkttdsk2020counter', function(data) {
                if(data.stts){
                    clearInterval(strt);

                    console.log(data.data)
                }
            }, 'json');
        }, 1000);*/
        $.ajax({
            url: baseUrl+'cek.php',
            // url: baseUrl+'documents/bulkttdsk2020counter',
            dataType:"json",
            success:function(data){
                $("#progress").html('<div class="bar" style="width:' + data.percent + '%;background-color: #49CB41 !important;"></div>');
                $("#message").html(data.message);
                // If the process is completed, we should stop the checking process.
                if (data.percent == 100) {
                    getDataNeedApproval();
                    getDataIsApprove();
                    window.clearInterval(timer);
                    timer = window.setInterval(function () {
                        $("#message").html("Completed");
                        window.clearInterval(timer);
                    }, 1000);
                }
            }
        });
    }


</script>
</body>

</html>
