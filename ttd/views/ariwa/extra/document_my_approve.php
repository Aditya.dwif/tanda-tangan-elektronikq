<style>
    .dataTables_wrapper .dataTables_paginate .paginate_button {
        padding : 0 !important;
        margin-left: 0 !important;
        display: inline !important;
        border: 0 !important;
        font-size: 10px !important;
    }
    .dataTables_wrapper .dataTables_paginate .pagination {
        margin: 0 !important;
    }
    .dataTables_filter {
        float: left !important;
    }
</style>
<table id="doc_my_approval_table" class="table table-responsive" style="height: 50px;">
    <thead>
    <tr>
        <th>File</th>
    </tr>
    </thead>
    <tbody>
    @if ($requestsData->count())
    <?php $hit=1 ?>
    @foreach ($requestsData as $isi)
    <tr>
        <td>
            <a href="{{ url('Document@open').$isi->document }}">
                <img width="40" style="float: left;" src="<?=url('');?>assets/images/formats/{{ $isi->extension }}.png">
                <div><b>{{ $isi->name }}</b></div>
                <small>sign at: {{ $isi->update_time2 }}</small>
            </a>
        </td>
    </tr>
    @endforeach
    @else
    <tr>
        <td><i><small>No Data</small></i></td>
    </tr>
    @endif
    </tbody>
</table>
