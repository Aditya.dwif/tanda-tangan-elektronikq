<style>

</style>
<div class="">
    <h4 class="text-info">Update Software</h4>
    <ul class="timeline">
        @if ($getversions->count())
            @foreach ($getversions as $isi)
                <li>
                    <h1>Version {{ $isi->version }}</h1>
                    <a href="#" class="float-right">{{ $isi->version_release_date2 }}</a>
                    <p>
                        Apa yang baru:
                    </p>
                    {{ $isi->ket }}
                </li>
            @endforeach
        @endif
        <li>
            <h1>Version 1.0</h1>
            <a href="#" class="float-right">5 April, 2020</a>
            <p>Rilis Versi Pertama</p>
        </li>
    </ul>
</div>
