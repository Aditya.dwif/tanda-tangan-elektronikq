
<link href="<?=url("");?>assets/ariwa/jqtree/jqtree.css" rel="stylesheet">
<script type="text/javascript">
</script>
<script src="<?=url("");?>assets/ariwa/jqtree/tree.jquery.js"></script>
<div>
    <div id="tree1"></div>
</div>

<script>
    var data = [
        {
            name: 'node1',
            children: [
                { name: 'child1' },
                { name: 'child2' }
            ]
        },
        {
            name: 'node2',
            children: [
                { name: 'child3' }
            ]
        }
    ];

    $(document).ready(function(){

        $('#tree1').tree({
            data: data
        });
    });
</script>
