<?php
use Simcify\Auth;
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ config('app.metadata')['title_desc'] }}">
    <meta name="author" content="{{ config('app.metadata')['title_desc'] }}">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=url("");?>uploads/app/{{ env('APP_ICON'); }}">
    <title>{{ $document->name }} | {{ config('app.metadata')['title'] }}</title>
    <!-- Ion icons -->
    <link href="<?=url("");?>assets/fonts/ionicons/css/ionicons.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=B612+Mono:400,400i,700|Charm:400,700|EB+Garamond:400,400i,700|Noto+Sans+TC:400,700|Open+Sans:400,400i,700|Pacifico|Reem+Kufi|Scheherazade:400,700|Tajawal:400,700&amp;subset=arabic" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?=url("");?>assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/select2/css/select2.min.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
    <link href="<?=url("");?>assets/css/simcify.min.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <!-- Signer CSS -->
    <link href="<?=url("");?>assets/css/style.css" rel="stylesheet">
    <style>
        .isipdf td, .isipdf th {
            vertical-align: top;
            border: 1px solid #aaa;
            padding: 3px;
        }

        #watermark {
            /*background-image: url("ariwa/images/finalDoc.png");*/
            background-repeat: repeat;
        }
    </style>


</head>

<body>
<div id="print_to_pdf">
    <div class="light-card widget">
        <div class="tab-content">
            <h3>History Dokumen</h3>
            <div id="history" class="tab-pane fade in active">

                <div class="timeline">
                    <div class="circle"></div>
                    <ul>
                        @foreach ( $history as $history )
                        <li class="{{ $history->type }}"><em class="text-xs">{{ date("F j, Y H:i", strtotime($history->time_)) }}</em> {{ $history->activity }}</li>
                        @endforeach
                    </ul>
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>
</div>

{{ view("includes/footer"); }}
</body>

</html>
