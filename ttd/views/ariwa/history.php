<?php
use Simcify\Auth;
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ config('app.metadata')['title_desc'] }}">
    <meta name="author" content="{{ config('app.metadata')['title_desc'] }}">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=url("");?>uploads/app/{{ env('APP_ICON'); }}">
    <title>{{ $document->name }} | {{ config('app.metadata')['title'] }}</title>
    <!-- Ion icons -->
    <link href="<?=url("");?>assets/fonts/ionicons/css/ionicons.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=B612+Mono:400,400i,700|Charm:400,700|EB+Garamond:400,400i,700|Noto+Sans+TC:400,700|Open+Sans:400,400i,700|Pacifico|Reem+Kufi|Scheherazade:400,700|Tajawal:400,700&amp;subset=arabic" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?=url("");?>assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/select2/css/select2.min.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
    <link href="<?=url("");?>assets/css/simcify.min.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <!-- Signer CSS -->
    <link href="<?=url("");?>assets/css/style.css" rel="stylesheet">
    <script src="<?=url("");?>assets/js/jscolor.js"></script>

    <style>
        .isipdf td, .isipdf th {
            vertical-align: top;
            border: 1px solid #aaa;
            padding: 3px;
        }

        #watermark {
            /*background-image: url("ariwa/images/finalDoc.png");*/
            background-repeat: repeat;
        }
    </style>


</head>

<body>
    <div id="print_to_pdf">
                <div class="light-card widget">
                    <h2><img src="<?=url("");?>assets/images/formats/pdf.png" class="img-responsive" style="width: 30px;float: left;"> {{ $document->name }}
                    </h2>
                    <div>Key: <b>{{ $document->document_key }}</b></div>
                </div>
                <div class="light-card widget">
                    <div class="tab-content">
                        <div style="margin-bottom: 10px;"><a class="btn btn-default" target="_blank" href="<?=url("");?>dochistpdf/{{ $document->document_key }}"><i class="ion-ios-cloud-download-outline"></i><span> Download Histori</span></a></div>
                        <h3>Histori Dokumen</h3>
                        <div id="history" class="tab-pane fade in active">

                            <div class="timeline">
                                <div class="circle"></div>
                                <ul>
                                    @foreach ( $history as $history )
                                    <li class="{{ $history->type }}"><em class="text-xs">{{ date("F j, Y H:i", strtotime($history->time_)) }}</em> {{ $history->activity }}</li>
                                    @endforeach
                                </ul>
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    <div style="padding: 10px !important;">
        <div class="light-card document">
            <div style="margin-bottom: 10px;"><a class="btn btn-default" href="{{ url('') }}uploads/files/{{ $document->filename }}" download="{{ $document->name.'.'.$document->extension }}"><i class="ion-ios-cloud-download-outline"></i><span> Download Dokumen</span></a></div>
            <div class="signer-document">
                <div class="document-pagination">
                    <div class="pull-left">
                        <div class="input-group mb-3">
                            <button id="prev" class="btn btn-default input-group-prepend"><i class="ion-ios-arrow-left"></i></button>
                            <select class="btn btn-default" id="prevnext_combobox" onchange="jumptopage($(this).val()-1)"></select>
                            <button id="next" class="btn btn-default input-group-prepend"><i class="ion-ios-arrow-right"></i></button>

                            <span class="text-muted ml-15">Page <span id="page_num">0</span> of <span id="page_count">0</span></span>
                        </div>
                    </div>
                    <div class="pull-right">
                        <button class="btn btn-default btn-round btn-zoom" zoom="plus"><i class="ion-plus"></i></button>
                        <button class="btn btn-default btn-round btn-zoom" zoom="minus"><i class="ion-minus"></i></button>

                    </div>
                </div>
                <div class="document-load">
                    <div class="loader-box"><div class="circle-loader"></div></div>
                </div>
                <div class="document-error">
                    <i class="ion-android-warning text-danger"></i>
                    <p class="text-muted"><strong>Oops! </strong> <span class="error-message"> Something went wrong.</span></p>
                </div>
                <div class="text-center">
                    <div class="document-map"></div>
                    <canvas id="document-viewer"></canvas>
                </div>
            </div>
        </div>
    </div>
    {{ view("includes/footer"); }}
</body>

</html>

<!-- scripts -->
<script src="<?=url("");?>assets/js/jquery-3.2.1.min.js"></script>
<script src="<?=url("");?>assets/libs/dropify/js/dropify.min.js"></script>
<script src="<?=url("");?>assets/libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=url("");?>assets/js/simcify.min.js"></script>


<script src="<?=url("");?>assets/libs/html2canvas/html2canvas.js"></script>
<script src="<?=url("");?>assets/libs/clipboard/clipboard.min.js"></script>
<script src="<?=url("");?>assets/libs/jquery-ui/jquery-ui.min.js"></script>
<script src="<?=url("");?>assets/libs/select2/js/select2.min.js"></script>
<script src="<?=url("");?>assets/libs/tagsinput/bootstrap-tagsinput.js"></script>
<script src="<?=url("");?>assets/js//jquery.slimscroll.min.js"></script>
<script src="<?=url("");?>assets/libs/jcanvas/jcanvas.min.js"></script>
<script src="<?=url("");?>assets/libs/jcanvas/signature.min.js"></script>
<script src="<?=url("");?>assets/libs/jcanvas/editor.min.js"></script>
<script src="<?=url("");?>assets/js/touch-punch.min.js"></script>
<script src="<?=url("");?>assets/js/pdf.js"></script>
<script type="text/javascript">
    var url = '<?=url("");?>uploads/files/{{ $document->filename }}',
        isTemplate = '{{ $document->is_template }}',
        signDocumentUrl = '<?=url("Guest@sign");?>',
        auth = false;
    createTemplateUrl = sendRequestUrl = postChatUrl = settingsPage = saveFieldsUrl = getChatUrl = deleteFieldsUrl = countNotificationsUrl = '',
        baseUrl = '<?=url("");?>',
        loginPage = '<?=url("Auth@get");?>';
    document_key = '{{ $document->document_key }}';
    PDFJS.workerSrc = '<?=url("");?>assets/js/pdf.worker.min.js';

    @if ( is_object($request) && $request->status == "Pending" )
    var signingKey = '{{ $request->signing_key }}';
    var requestPositions = {{ $requestPositions }};
    var requestWidth = {{ $requestWidth }};
    @else
    var signingKey = '';
    @endif


</script>
<!-- custom scripts -->
<script src="<?=url("");?>assets/js/app.js"></script>
<script src="<?=url("");?>assets/js/signer.js"></script>
<script src="<?=url("");?>assets/js/render.js"></script>
