@if($attchmnt->count())
    <table class="table" width="100%">
    @foreach($attchmnt as $isi)
        <tr style="vertical-align: top;">
            <td>
                @if($isi->created_by==Simcify\Auth::user()->id)
                <button onclick="removefile(event, '{{ $isi->id }}')"><i class="ion-ios-trash-outline"></i></button>
                @else
                -
                @endif
            </td>
            <td width="95%" class="ariwa-wrap">
                <a target="_blank" href="{{ url('').'attachment/download/'.str_replace('.', '___', $isi->f_file_name) }}">{{ Ariwa\Fungsional::getIconDocument($isi->f_ext) }} {{ $isi->f_name }}</a>
                <br>
                <em class="text-xs"><b>{{ file_size($isi->f_size) }}</b> by: <b>{{ $isi->fname.' '.$isi->lname }}</b>, {{ $isi->created_at2 }}</em>
            </td>
        </tr>
    @endforeach
    </table>
@else
    <small><i>Empty</i></small>
@endif