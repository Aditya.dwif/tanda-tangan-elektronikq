<?php $document = $data['document']?>
<link href="<?=url("");?>assets/ariwa/filer/css/jquery.filer.css" rel="stylesheet">

<style>
    .btn.btn-xs {
        line-height: 22px !important;
        padding: 1px 10px !important;
    }
</style>
<div class="timeline">
    <div>
        <button style="margin-bottom: 10px;" onclick="showAttachment(event)" class="btn btn-success btn-xs"><i class="ion-ios-cloud-upload-outline"></i> Add File</button>
        <br>
        <div id="attachment_content"></div>
    </div>

</div>

<script>
</script>



<!-- Rename file Modal -->
<div class="modal fade" id="attachment_modal" role="dialog">
    <div class="close-modal" data-dismiss="modal">&times;</div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Upload Attachment</h4>
            </div>
            <form id="attachment_form" data-parsley-validate="" method="POST">
                <div class="modal-body">
                    <p class="text-muted">Pilih 1 atau lebih file.</p>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="file" name="files[]" id="filer_input" multiple="multiple" required>
                                <input type="hidden" name="csrf-token" value="<?=csrf_token();?>">
                                <input type="hidden" name="document_key" value="{{ $document->document_key }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Upload</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    var maxPerFile = {{ config('app.ATTCHMENT_MAX_PER_FILE') }};
    var maxPerUpload = {{ config('app.ATTCHMENT_MAX_PER_UPLOAD') }};
</script>
<!-- Jvascript -->
<script src="<?=url("");?>assets/ariwa/filer/js/jquery.filer.min.js" type="text/javascript"></script>
<script src="<?=url("");?>assets/ariwa/filer/examples/default/js/custom.js" type="text/javascript"></script>

<script>
    $(function() {
    });
    function getAttachment(){
        $.ajax({
            url:'<?=url("");?>attachment',
            data:{ document_key: document_key, 'csrf-token': '<?=csrf_token();?>' },
            type:'POST',
            dataType:"html",
            success:function(response){
                $('#attachment_content').html(response);
            },
            error:function(data){
                $('#attachment_content').html('-');
            }
        });
    }
    function showAttachment(e) {
        e.preventDefault();
        $('#attachment_modal').modal('show');
        $('#filer_input').prop("jFiler").reset();
    }

    $('#attachment_form').on('submit',function(e) {
        if ( $(this).parsley().isValid() ) {
            showLoader();
            $.ajax({
                url:'<?=url("");?>attachment/save',
                data: new FormData(this),
                type:'POST',
                dataType:"json",

                contentType: false,
                cache: false,
                processData:false,

                success:function(data){
                    hideLoader();
                    getAttachment();
                    $('#attachment_modal').modal('hide');
                },
                error:function(data){
                    hideLoader();
                }
            });
        }
        e.preventDefault();
    });

    function removefile(e, id = ''){
        if(!confirm("Anda Yakin ?")){
            return false;
        }
        $.ajax({
            url:'<?=url("");?>attachment/del',
            data:{ id: id, 'csrf-token': '<?=csrf_token();?>' },
            type:'POST',
            dataType:"json",
            success:function(response){
                getAttachment();
            },
            error:function(data){
            }
        });
    }
</script>