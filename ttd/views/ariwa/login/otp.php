<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=url("");?>uploads/app/{{ env('APP_ICON'); }}">
    <title>OTP | Tanda Tangan Digital Online</title>
    <!-- Ion icons -->
    <link href="<?=url("");?>assets/fonts/ionicons/css/ionicons.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?=url("");?>assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=url("");?>assets/css/simcify.min.css" rel="stylesheet">
    <!-- Signer CSS -->
    <link href="<?=url("");?>assets/css/style.css" rel="stylesheet">
    <style type="text/css">
        body {
            background-color: #42a5f5;
        }
        body:before {
            content: '';
            /*position: absolute;*/
            background: linear-gradient(rgba(255,255,255,0.7) 240px, #f2f7fb 0%);
            top: 0;
            bottom: 0;
            left: 0;
            width: 100%;
            position: fixed;
            z-index: -1;
        }
    </style>

</head>

<body>
<div class="text-center mt-30">
    <img src="<?=url("");?>uploads/app/{{ env('APP_LOGO'); }}" width="150px">
</div>
<div class="login-card mb-30" style="margin-top: 25px !important;">
    <div class="sign-in">
        <h3 class="mb-30">Login</h3>
        <form id="form_otp" class="text-left" data-parsley-validate="" loader="true" method="POST">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>OTP</label>
                        <input type="hidden" name="csrf-token" value="<?=htmlentities(csrf_token(), ENT_QUOTES, 'UTF-8');?>" />
                        <input type="Password" class="form-control" name="otp" placeholder="otp" required value="">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-primary pull-right" type="submit">Submit</button>
                        <a class="btn btn-danger pull-right" href="{{ url('/signout') }}">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="copyright">
        <p class="text-center"><?=date("Y")?> &copy; <?=env("APP_NAME")?> | All Rights Reserved.</p>
    </div>
</div>

<!-- scripts -->
<script src="<?=url("");?>assets/js/jquery-3.3.1.min.js"></script>
<script src="<?=url("");?>assets/libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=url("");?>assets/ariwa/jquery-validation/jquery.validate.min.js"></script>
<script src="<?=url("");?>assets/js/parsley.min.js"></script>


<script>
    var a_intvl = null;
    function setTimer(t) {
        $('#b_otp').prop('disabled', true);
        $('#b_otp').html('Loading..');
        var timer2 = "00:10";
        a_intvl = setInterval(function() {
            var timer = timer2.split(':');
            //by parsing integer, I avoid all extra string processing
            var minutes = parseInt(timer[0], 10);
            var seconds = parseInt(timer[1], 10);
            --seconds;
            minutes = (seconds < 0) ? --minutes : minutes;
            seconds = (seconds < 0) ? 59 : seconds;
            seconds = (seconds < 10) ? '0' + seconds : seconds;
            //minutes = (minutes < 10) ?  minutes : minutes;

            if (minutes < 0){
                clearInterval(a_intvl);
                $('#b_otp').prop('disabled', false);
                $('#b_otp').html('Kirim Ulang OTP');
            }else{
                $('#b_otp').prop('disabled', true);
                $('#b_otp').html('Kirim Ulang OTP ('+minutes + ':' + seconds+')');
            }
            timer2 = minutes + ':' + seconds;
        }, 1000);
    }

</script>
<script>
    $('#form_otp').submit(function(event) {
        event.preventDefault();
        $(this).parsley().validate();
        if (($(this).parsley().isValid())) {
            showLoader();
            $.ajax({
                url: "{{ url('/cekotppost') }}",
                type: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function (response) {
                    hideLoader();
                    if(response.stts){
                        $('body').html('Loading..');
                        window.location.reload();
                    }else{
                        alert(response.msg);
                    }
                },
                error: function (xhr, status, error) {
                    if (loader) {
                        hideLoader();
                    }
                }
            });
        }
    });

    function submitotp(e) {
        e.preventDefault();
        $('#sms_code').val($('#m_otp_code').val());
        $('#form_registrasi').submit();
    }

    function kirimulangotp(e) {
        var indata = $('#form_registrasi').serializeArray();
        indata.splice(5,1);
        indata.splice(8,1);

        e.preventDefault();
        $('#b_otp').prop('disabled', true);
        $('#b_otp').html('Loading..');
        $.ajax({
            url:'<?=url("");?>validasi/otpulang',
            data:{ sms_id: $('#sms_id').val(), 'csrf-token': '<?=htmlentities(csrf_token(), ENT_QUOTES, 'UTF-8');?>', indata: indata },
            type:'POST',
            dataType:"json",
            success:function(response){
                if (response.status === "error") {
                    toastr.error(response.message, response.title);
                    return false;
                }
                $('#sms_id').val(response.sms_id);
                setTimer(1);
            },
            error:function(data){

            }
        });
    }


    function showLoader(options) {
        /**
         * remove existing loaders
         */
        hideLoader();
        if (options === undefined) {
            options = {
                color: "#007bff",
                background: "rgba(298, 294, 290, 0.9)"
            };
        } else {
            if (options.background === undefined) {
                options.background = "rgba(298, 294, 290, 0.9)";
            }
            if (options.color === undefined) {
                options.color = "#007bff";
            }
        }
        $("body").append('<div class="loading-overlay" style="background-color:' + options.background + ';"><div class="loader-box"><div class="circle-loader"></div></div></div>');
        $("body").append('<style class="notify-styling">.circle-loader:before { border-top-color: ' + options.color + '; }</style>');
    }
    /*
     * Simcyfy hide loader
     */
    function hideLoader() {
        $(".loading-overlay, notify-styling").remove();
    }
</script>

</body>

</html>
