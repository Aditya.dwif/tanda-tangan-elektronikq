<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ config('app.metadata')['title_desc'] }}">
    <meta name="author" content="{{ config('app.metadata')['title_desc'] }}">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=url("");?>uploads/app/{{ env('APP_ICON'); }}">
    <title>Setting | {{ config('app.metadata')['title'] }}</title>
    <!-- Ion icons -->
    <link href="<?=url("");?>assets/fonts/ionicons/css/ionicons.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash|Cookie|Courgette|Dr+Sugiyama|Grand+Hotel|Great+Vibes|League+Script|Meie+Script|Miss+Fajardose|Niconne|Pacifico|Petit+Formal+Script|Rochester|Sacramento|Tangerine" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?=url("");?>assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=url("");?>assets/css/simcify.min.css" rel="stylesheet">
    <!-- Signer CSS -->
    <link href="<?=url("");?>assets/css/style.css" rel="stylesheet">
    <script src="<?=url("");?>assets/js/jscolor.js"></script>
</head>

<body>

    <!-- header start -->
    {{ view("includes/header", $data); }}

    <!-- sidebar -->
    {{ view("includes/sidebar", $data); }}
    
    <div class="content">
        <div class="page-title">
            <h3>Settings</h3>
        </div>
        <div class="">
            <div class="settings-menu">
                <ul>
                    <li class="active"><a data-toggle="tab" href="#profile">Profile</a></li>
                    <li><a data-toggle="tab" href="#signature">Tanda tangan</a></li>
                    <li><a data-toggle="tab" href="#paraf">Paraf</a></li>
                    @if ( $user->role == "superadmin" || $user->role == "admin" )
                    <li><a data-toggle="tab" href="#company">Company</a></li>
                    <!--<li><a data-toggle="tab" href="#reminders">Reminders</a></li>-->
                    @endif
                    @if ( $user->role == "superadmin" ) 
                    <li><a data-toggle="tab" href="#system">Aplikasi</a></li>
                    @endif
                    <li><a data-toggle="tab" href="#password">Password</a></li>
                    @if(env('PIN_TTD', false))
                    <li><a data-toggle="tab" href="#setpin">PIN</a></li>
                    @endif
                </ul>
            </div>
            <div class="settings-forms">
                <div class="col-md-5 tab-content">
                    <!-- Profile start -->
                    <div id="profile" class="tab-pane fade in active">
                        <h4>Profile</h4>
                        <form class="simcy-form"action="<?=url("Settings@updateprofile");?>" data-parsley-validate="" loader="true" method="POST" enctype="multipart/form-data">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Profile picture</label><br>
                                        <small>Klik untuk Upload / Edit gambar</small>
                                        @if( !empty($user->avatar) )
                                        <input type="file" name="avatar" class="croppie" default="<?=url("");?>uploads/avatar/{{ $user->avatar }}" crop-width="200" crop-height="200" accept="image/*">
                                        @else
                                        <input type="file" name="avatar" class="croppie" crop-width="200" crop-height="200" accept="image/*">
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Nama Lengkap</label>
                                        <input type="text" class="form-control" name="fname" value="{{ $user->fname }}" placeholder="First name" required>
                                        <input type="hidden" class="form-control" name="lname" value="{{ $user->lname }}" placeholder="Last name">
                                        <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Email</label>
                                        <input disabled type="email" class="form-control" name="email" value="{{ $user->email }}" placeholder="Email address">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Nomor HP</label>
                                        <input disabled type="text" class="form-control" name="phone" value="{{ $user->phone }}" placeholder="Phone number" id="phone"  maxlength="13" minlength="10">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Alamat</label>
                                        <input type="text" class="form-control" name="address" value="{{ $user->address }}" placeholder="Alamat">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-primary" type="submit" >Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                    <!-- profile end -->
                    @if ( $user->role == "superadmin" || $user->role == "admin" ) 
                    <!-- Company start -->
                    <div id="reminders" class="tab-pane fade">
                        <h4>Reminders</h4>
                        <p>Reminders are emails sent to someone when no action has been taken by them after a signing request had been sent.
                            The emails will be sent after the sent number of days</p>
                        <form class="simcy-form"action="<?=url("Settings@updatereminders");?>" data-parsley-validate="" loader="true" method="POST">
                            <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        @if( $company->reminders == "On" )
                                        <input type="checkbox" id="enable-reminders" class="switch" name="reminders" value="On" checked />
                                        @else
                                        <input type="checkbox" id="enable-reminders" class="switch" name="reminders" value="Off" />
                                        @endif
                                        <label for="enable-reminders">Enable reminders</label>
                                    </div>
                                </div>
                            </div>
                            @if( $company->reminders == "On" )
                            <div class="panel-group reminders-holder" id="accordion">
                            @else
                            <div class="panel-group reminders-holder" id="accordion" style="display: none;">
                            @endif
                                @if( count($reminders) > 0 )
                                @foreach ($reminders as $index => $reminder)
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        @if( $index >  0 )
                                        <span class="delete-reminder" data-toggle="tooltip" title="Remove reminder"><i class="ion-ios-trash"></i></span>
                                        @endif
                                        <h4 class="panel-title"><a data-parent="#accordion" data-toggle="collapse" href="#collapse{{ $index + 1 }}">Reminder #<span class="count">{{ $index + 1 }}</span></a></h4>
                                    </div>
                                    @if( $index ==  0 )
                                    <div class="panel-collapse collapse in" id="collapse{{ $index + 1 }}">
                                    @else
                                    <div class="panel-collapse collapse" id="collapse{{ $index + 1 }}">
                                    @endif
                                        <div class="panel-body">
                                            <div class="remider-item">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input type="hidden" name="count[]" value="1">
                                                            <label>Email subject</label> <input class="form-control" name="subject[]" placeholder="Email subject" required type="text" value="{{ $reminder->subject }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label>Days after request is sent</label> <input class="form-control" name="days[]" min="1" placeholder="Days after request is sent" required type="number" value="{{ $reminder->days }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label>Message</label>
                                                            <textarea class="form-control" name="message[]" required rows="9">{{ $reminder->message }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @else
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><a data-parent="#accordion" data-toggle="collapse" href="#collapse1">Reminder #<span class="count">1</span></a></h4>
                                    </div>
                                    <div class="panel-collapse collapse in" id="collapse1">
                                        <div class="panel-body">
                                            <div class="remider-item">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input type="hidden" name="count[]" value="1">
                                                            <label>Email subject</label> <input class="form-control" name="subject[]" placeholder="Email subject" required type="text" value="Signing invitation reminder ">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label>Days after request is sent</label> <input class="form-control" name="days[]" min="1" placeholder="Days after request is sent" required type="number" value="3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label>Message</label>
                                                            <textarea class="form-control" name="message[]" required rows="9">Hello there, 

I hope you are doing well.
I am writing to remind you about the signing request I had sent earlier.

Cheers!
{{ $user->fname }} {{ $user->lname }}
</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <span class="delete-reminder" data-toggle="tooltip" title="Remove reminder"><i class="ion-ios-trash"></i></span>
                                        <h4 class="panel-title"><a data-parent="#accordion" data-toggle="collapse" href="#collapse2">Reminder #<span class="count">2</span></a></h4>
                                    </div>
                                    <div class="panel-collapse collapse " id="collapse2">
                                        <div class="panel-body">
                                            <div class="remider-item">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input type="hidden" name="count[]" value="1">
                                                            <label>Email subject</label> <input class="form-control" name="subject[]" placeholder="Email subject" required type="text" value="Signing invitation reminder ">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label>Days after request is sent</label> <input class="form-control" name="days[]" min="1" placeholder="Days after request is sent" required type="number" value="7">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label>Message</label>
                                                            <textarea class="form-control" name="message[]" required rows="9">Hello there, 

I hope you are doing well.
I am writing to remind you about the signing request I had sent earlier.

Cheers!
{{ $user->fname }} {{ $user->lname }}
</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <span class="delete-reminder" data-toggle="tooltip" title="Remove reminder"><i class="ion-ios-trash"></i></span>
                                        <h4 class="panel-title"><a data-parent="#accordion" data-toggle="collapse" href="#collapse3">Reminder #<span class="count">3</span></a></h4>
                                    </div>
                                    <div class="panel-collapse collapse " id="collapse3">
                                        <div class="panel-body">
                                            <div class="remider-item">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input type="hidden" name="count[]" value="1">
                                                            <label>Email subject</label> <input class="form-control" name="subject[]" placeholder="Email subject" required type="text" value="Signing invitation reminder">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label>Days after request is sent</label> <input class="form-control" name="days[]" min="1" placeholder="Days after request is sent" required type="number" value="7">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label>Message</label>
                                                            <textarea class="form-control" name="message[]" required rows="9">Hello there,

I hope you are doing well.
I am writing to remind you about the signing request I had sent earlier.

Cheers!
{{ $user->fname }} {{ $user->lname }}
</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-default add-reminder" type="button">Add reminder</button>
                                        <button class="btn btn-primary" type="submit">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                    <!-- reminder end -->
                    <!-- Company start -->
                    <div id="company" class="tab-pane fade">
                        <h4>Company</h4>
                        <form class="simcy-form"action="<?=url("Settings@updatecompany");?>" data-parsley-validate="" loader="true" method="POST">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Company name</label>
                                        <input type="text" class="form-control" name="name" placeholder="Company name" value="{{ $company->name }}" required>
                                        <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Email address</label>
                                        <input type="email" class="form-control" name="email" placeholder="Email address" value="{{ $company->email }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Phone number</label>
                                        <input type="text" class="form-control" name="phone" placeholder="Phone number" value="{{ $company->phone }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-primary" type="submit">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                    <!-- Company end -->
                    @endif
                    @if ( $user->role == "superadmin" ) 
                    <!-- System start -->
                    <div id="system" class="tab-pane fade">
                        <h4>Aplikasi</h4>
                        <form class="simcy-form"action="<?=url("Settings@updatesystem");?>" data-parsley-validate="" loader="true" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Nama Aplikasi</label>
                                        <input type="text" class="form-control system-name" placeholder="Application name" name="APP_NAME" value="{{ env('APP_NAME'); }}" required>
                                        <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Logo Aplikasi</label><br>
                                        <small>Klik untuk Upload / Edit gambar</small>
                                        <input type="file" name="APP_LOGO" class="croppie" default="<?=url("");?>uploads/app/{{ env('APP_LOGO'); }}" crop-width="541" crop-height="152" accept="image/*">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Icon </label><br>
                                        <small>Klik untuk Upload / Edit gambar</small>
                                        <input type="file" name="APP_ICON" class="croppie" default="<?=url("");?>uploads/app/{{ env('APP_ICON'); }}" crop-width="152" crop-height="152" accept="image/*">
                                    </div>
                                </div>
                            </div>
                            <div class="divider"></div>

                            <div class="divider"></div>
                            <h5>Personal Accounts</h5>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Disk Limit (MBs)</label>
                                        <input type="number" class="form-control" name="PERSONAL_DISK_LIMIT" placeholder="Disk Limit (MBs)" value="{{ env('PERSONAL_DISK_LIMIT'); }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>File Limit</label>
                                        <input type="number" class="form-control" name="PERSONAL_FILE_LIMIT" placeholder="File Limit" value="{{ env('PERSONAL_FILE_LIMIT'); }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-primary" type="submit">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                    <!-- system end -->
                    @endif
                    <!-- Signature start -->
                    <div id="signature" class="tab-pane fade">
                        <h4>Tanda tangan</h4>
                        <p>Buat tanda tangan anda disini. Klik "<b>Update Tanda tangan</b>" dibawah</p>
                        <div class="row">
                            <div class="col-md-12">
                             <div class="signature-holder">
                                <div class="signature-body">
                                    @if ( empty( $user->signature ) )
                                    <img src="<?=url("");?>uploads/signatures/demo.png" class="img-responsive">
                                    @else
                                    <img src="<?=url("");?>uploads/signatures/{{ $user->signature }}" class="img-responsive">
                                    @endif
                                </div>
                            </div>
                            <div class="signature-btn-holder">
                                <button class="btn btn-primary btn-block"  data-toggle="modal" onclick="seturlgbttd('ttd')" data-target="#updateSignature" data-target="#createFolder" data-backdrop="static" data-keyboard="false"> Update Tanda tangan</button>
                            </div>
                            </div>
                        </div>
                    </div>

                    <div id="paraf" class="tab-pane fade">
                        <h4>Paraf</h4>
                        <p>Buat Paraf anda disini. Klik "<b>Update Paraf</b>" dibawah</p>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="signature-holder">
                                    <div class="signature-body2">
                                        @if ( empty( $user->paraf ) )
                                        <img src="<?=url("");?>uploads/signatures/demo.png" class="img-responsive paraf">
                                        @else
                                        <img src="<?=url("");?>uploads/paraf/{{ $user->paraf }}" class="img-responsive paraf">
                                        @endif
                                    </div>
                                </div>
                                <div class="signature-btn-holder">
                                    <button class="btn btn-primary btn-block" onclick="seturlgbttd('paraf')"  data-toggle="modal" data-target="#updateSignature" data-backdrop="static" data-keyboard="false"> Update Paraf</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- password end -->
                    <!-- password start -->
                    <div id="password" class="tab-pane fade">
                        <h4>Password</h4>
                        <form class="simcy-form"action="<?=url("Settings@updatepassword");?>" data-parsley-validate="" loader="true" method="POST">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Password Saat Ini</label>
                                        <input type="password" class="form-control" name="current" required placeholder="Current password">
                                        <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Password Baru</label>
                                        <input type="password" class="form-control" name="password" data-parsley-required="true" data-parsley-minlength="6" data-parsley-error-message="Password is too short!" id="newPassword" placeholder="New password">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Ulangi Password Baru</label>
                                        <input type="password" class="form-control" data-parsley-required="true" data-parsley-equalto="#newPassword" data-parsley-error-message="Passwords don't Match!" placeholder="Confirm password">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-primary" type="submit">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                    <!-- password end -->

                    @if(env('PIN_TTD', false))
                    <div id="setpin" class="tab-pane fade">
                        @if($user->pin)
                        <h4>Update PIN <small>( 6 Angka )</small></h4>
                        @else
                        <h4>Set PIN <small>( 6 Angka )</small></h4>
                        @endif
                        <form id="pin_form" data-parsley-validate="true" method="POST">
                            <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                            @if($user->pin)
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>PIN Saat Ini</label>
                                        <input type="number" style="-webkit-text-security: disc;" class="form-control" name="current_pin" required placeholder="Current PIN">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Isikan PIN Baru Anda</label>
                                        <input type="number" style="-webkit-text-security: disc;" class="form-control" name="pin" data-parsley-required="true" data-parsley-minlength="6" data-parsley-maxlength="6" data-parsley-error-message="Pin harus 6 angka!" id="pin" placeholder="Input PIN">
                                        <input type="hidden" name="pin_is_new" value="0">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Ulangi PIN Baru Anda</label>
                                        <input type="number" style="-webkit-text-security: disc;" class="form-control" name="pin2" data-parsley-required="true" data-parsley-minlength="6" data-parsley-maxlength="6" data-parsley-equalto="#pin" data-parsley-error-message="PIN tidak sama." id="pin2" placeholder="Ulangi PIN">
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Isikan PIN Anda</label>
                                        <div class=" input-group">
                                            <input type="number" style="-webkit-text-security: disc;" class="form-control" name="pin" data-parsley-required="true" data-parsley-minlength="6" data-parsley-maxlength="6" data-parsley-error-message="Pin harus 6 angka!" id="pin" placeholder="Input PIN">
                                            <input type="hidden" name="pin_is_new" value="1">
                                            <div class="input-group-btn">
                                                <button class="btn btn-default" type="button">
                                                    <i class="glyphicon glyphicon-eye-open"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Ulangi PIN Anda</label>
                                        <div class=" input-group">
                                            <input type="number" style="-webkit-text-security: disc;" class="form-control" name="pin2" data-parsley-required="true" data-parsley-minlength="6" data-parsley-maxlength="6" data-parsley-equalto="#pin" data-parsley-error-message="PIN tidak sama." id="pin2" placeholder="Ulangi PIN">
                                            <div class="input-group-btn">
                                                <button class="btn btn-default" type="button">
                                                    <i class="glyphicon glyphicon-eye-open"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-primary" type="submit">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                    @endif
                </div>
            </div>
        </div>

    </div>


      <!-- Upload file Modal -->
    <div class="modal fade" id="updateSignature" role="dialog">
        <div class="close-modal" data-dismiss="modal">&times;</div>
    <div class="modal-dialog">
      <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="updateSignatureTitle">Update Tanda tangan </h4>
                </div>
      <ul class="head-links">
        <li type="capture" class="active"><a data-toggle="tab" href="#text">Text</a></li>
        <li type="upload"><a data-toggle="tab" href="#upload">Upload</a></li>
        <li type="draw"><a data-toggle="tab" href="#draw">Draw</a></li>
      </ul>
        <div class="modal-body">
        <div class="tab-content">
            <div id="text" class="tab-pane fade in active">
                      <form>
                          <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                  <label>Ketik Disini</label>
                                  <input type="text" class="form-control signature-input" name="" placeholder="Ketik Disini" maxlength="18" value="Your Name">
                                </div>
                                <div class="col-md-6">
                                  <label>Pilih Huruf</label>
                                  <select class="form-control signature-font" name="">
                                      <option value="Lato">Lato</option>
                                      <option value="Miss Fajardose">Miss Fajardose</option>
                                      <option value="Meie Script">Meie Script</option>
                                      <option value="Petit Formal Script">Petit Formal Script</option>
                                      <option value="Niconne">Niconne</option>
                                      <option value="Rochester">Rochester</option>
                                      <option value="Tangerine">Tangerine</option>
                                      <option value="Great Vibes">Great Vibes</option>
                                      <option value="Berkshire Swash">Berkshire Swash</option>
                                      <option value="Sacramento">Sacramento</option>
                                      <option value="Dr Sugiyama">Dr Sugiyama</option>
                                      <option value="League Script">League Script</option>
                                      <option value="Courgette">Courgette</option>
                                      <option value="Pacifico">Pacifico</option>
                                      <option value="Cookie">Cookie</option>
                                      <option value="Grand Hotel">Grand Hotel</option>
                                  </select>
                                </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                  <label>Ketebalan</label>
                                  <select class="form-control signature-weight" name="">
                                      <option value="normal">Regular</option>
                                      <option value="bold">Bold</option>
                                      <option value="lighter">Lighter</option>
                                  </select>
                                </div>
                                <div class="col-md-4">
                                  <label>Warna</label>
                                  <input  class="form-control signature-color jscolor { valueElement:null,borderRadius:'1px', borderColor:'#e6eaee',value:'000000',zIndex:'99999', onFineChange:'updateSignatureColor(this)'}" readonly="">
                                </div>
                                <div class="col-md-4">
                                  <label>Model</label>
                                  <select class="form-control signature-style" name="">
                                      <option value="normal">Regular</option>
                                      <option value="italic">Italic</option>
                                  </select>
                                </div>
                            </div>
                          </div>
                      </form>
                      <div class="divider"></div>
                      <h4 class="text-center">Preview</h4>
                      <div class="text-signature-preview">
                          <div class="text-signature" id="text-signature" style="color: #000000">Your Name</div>
                      </div>

            </div>
            <div id="upload" class="tab-pane fade">
                <p>Upload gambar tanda tangan Anda.<br>
                    <small>Saran: gunakan gambar transparan dalam bentuk file "<b>.png</b>").</small>
                </p>
                  <div class="form-group">
                        <div class="row">
                          <div class="col-md-12">
                            <label>Upload Gambar</label>
                                <input type="file" name="signatureupload" class="croppie" crop-width="400" crop-height="150">
                          </div>
                      </div>
                  </div>
            </div>
            <div id="draw" class="tab-pane fade text-center">
                <p>Gunakan <i>stylus-pen</i> dan/atau layar sentuh untuk hasil yang baik.</p>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="draw-signature-holder"><canvas width="400" height="150" id="draw-signature"></canvas></div>
                            <div class="signature-tools text-center" id="controls">
                                <div class="signature-tool-item with-picker">
                                    <div><button class="jscolor { valueElement:null,borderRadius:'1px', borderColor:'#e6eaee',value:'000000',zIndex:'99999', onFineChange:'modules.color(this)'}"></button></div>
                                </div>
                                <div class="signature-tool-item" id="signature-stroke" stroke="5">
                                    <div class="tool-icon tool-stroke"></div>
                                </div>
                                <div class="signature-tool-item" id="undo">
                                    <div class="tool-icon tool-undo"></div>
                                </div>
                                <div class="signature-tool-item" id="clear">
                                    <div class="tool-icon tool-erase"></div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary save-signature">Save Signature</button>
        </div>
      </div>
      
    </div>
  </div>



    <!-- footer -->
    {{ view("includes/footer"); }}

    <!-- scripts -->
    <script>
        var fullName = "{{ $user->fname }} {{ $user->lname }}",
              saveSignatureUrl = "<?=url("Signature@save");?>",
              auth = true;
    </script>
    <script src="<?=url("");?>assets/js/jquery-3.2.1.min.js"></script>
    <script src="<?=url("");?>assets/libs/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=url("");?>assets/js//jquery.slimscroll.min.js"></script>
    <script src="<?=url("");?>assets/libs/html2canvas/html2canvas.js"></script>
    <script src="<?=url("");?>assets/libs/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?=url("");?>assets/libs/jcanvas/jcanvas.min.js"></script>
    <script src="<?=url("");?>assets/libs/jcanvas/signature.min.js"></script>
    <script src="<?=url("");?>assets/js/simcify.min.js"></script>

    <!-- custom scripts -->
    <script src="<?=url("");?>assets/js/app.js"></script>

<script>
    $(function() {
        $('#phone').on('input blur paste', function(){
            $(this).val($(this).val().replace(/\D/g, '').toString());
        });
    });
    // validator
    window.Parsley.addValidator('mailmdm', {
        validateString: function(value) {
            var m = value.split('@');
            if(m[1]!='mdmedia.co.id'){
                return false;
            }
            return true;
        },
        messages: {
            en: 'Email yang digunakan harus @mdmedia.co.id',
        }
    });

    function seturlgbttd(par = 'ttd'){ // ttd / paraf
        if(par == 'ttd'){
            $('#updateSignatureTitle').html('Update Tanda tangan');
            saveSignatureUrl = "<?=url("Signature@save");?>";
        }else{
            $('#updateSignatureTitle').html('Update Paraf');
            saveSignatureUrl = "<?=url("Signature@saveparaf");?>";
        }
    }

    function parafCallback(image){
        $("img.paraf").attr("src", image);
        $('#updateSignature').modal('hide');
    }

    /*function saveparaf(e) {
        e.preventDefault();
        var paraf = $('#draw-paraf').getCanvasImage('png');
        if (auth) {
            server({
                url: "",
                data: {
                    "paraf": paraf,
                    "csrf-token": Cookies.get("CSRF-TOKEN")
                },
                loader: true
            });
        }else{
            $('#updateSignature').modal('hide');
            toastr.success("Signature successfully saved.","Alright!", {timeOut: 2000, closeButton: true, progressBar: false});
        }
    }*/

    /*@if( !empty($user->avatar) )
    <input type="file" name="avatar" class="croppie" default="/uploads/avatar/{{ $user->avatar }}" crop-width="200" crop-height="200" accept="image/!*">
        @else
    <input type="file" name="avatar" class="croppie" crop-width="200" crop-height="200" accept="image/!*">
        @endif*/

    <?php if(env('PIN_TTD', false)){ ?>
    $('#pin_form').on('submit',function(e) {
        if ( $(this).parsley().isValid() ) {
            showLoader();
            $.ajax({
                url:'<?=url("");?>settings/update/pin',
                data: new FormData(this),
                type:'POST',
                dataType:"json",

                contentType: false,
                cache: false,
                processData:false,

                success:function(data){
                    hideLoader();
                    serverResponse(data);
                },
                error: function (xhr, status, error) {
                    if (loader) {
                        hideLoader();
                    }
                    toastr.error(error, "Oops!");
                }
            });
        }
        e.preventDefault();
    });
    <?php } ?>

</script>

</body>

</html>

