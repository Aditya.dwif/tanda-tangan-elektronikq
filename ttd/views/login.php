<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ config('app.metadata')['title_desc'] }}">
    <meta name="author" content="{{ config('app.metadata')['title_desc'] }}">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=url("");?>uploads/app/{{ env('APP_ICON'); }}">
    <title>Login | {{ config('app.metadata')['title'] }}</title>
    <!-- Ion icons -->
    <link href="<?=url("");?>assets/fonts/ionicons/css/ionicons.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?=url("");?>assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=url("");?>assets/css/simcify.min.css" rel="stylesheet">
    <!-- Signer CSS -->
    <link href="<?=url("");?>assets/css/style.css" rel="stylesheet">
    <style type="text/css">
        body {
            background-color: #42a5f5;
        }
        body:before {
            content: '';
            /*position: absolute;*/
            background: linear-gradient(rgba(255,255,255,0.7) 240px, #f2f7fb 0%);
            top: 0;
            bottom: 0;
            left: 0;
            width: 100%;
            position: fixed;
            z-index: -1;
        }
    </style>

    <script src='https://www.google.com/recaptcha/api.js' async defer></script>

</head>

<body>
<div class="text-center mt-30">
    <img src="<?=url("");?>uploads/app/{{ env('APP_LOGO'); }}" width="150px">
</div>
<div class="login-card mb-30" style="margin-top: 25px !important;">
    @if ( $guest AND env('GUEST_SIGNING') == "Enabled" )
    <a class="btn btn-block btn-success m-t-50" href="{{ $signingLink }}">Sign as a Guest</a>
    @endif
    <div class="sign-in">
        <h3 class="mb-30">Login</h3>
        <form class="text-left simcy-form" action="<?=url("Auth@signin");?>" data-parsley-validate="" loader="true" method="POST">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email" placeholder="Email" required value="user01@ariwa.me">
                        <input type="hidden" name="csrf-token" value="<?=htmlentities(csrf_token(), ENT_QUOTES, 'UTF-8');?>" />
                        <input type="hidden" name="url" value="{{ $url }}" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>Password</label>
                        <input type="Password" class="form-control" name="password" placeholder="Password" required value="123456">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <p class="pull-left m-t-5"><a href="" target="forgot-password">Lupa password?</a></p>
                        <button class="btn btn-primary pull-right" type="submit" name="login">Login</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    @if ( env('NEW_ACCOUNTS') == "Enabled" )
    <div class="sign-up" style="display: none;">
        <h5 class="mb-30">Create a free account</h5>

        <form class="text-left form" id="form_registrasi" action="<?=url("Auth@signup");?>" data-parsley-validate="" loader="true" method="POST">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>First name</label>
                        <input type="text" class="form-control" name="fname" placeholder="First name" required>
                        <input type="hidden" name="csrf-token" value="<?=htmlentities(csrf_token(), ENT_QUOTES, 'UTF-8');?>" />
                        <input type="hidden" name="sms_id" id="sms_id"/>
                        <input type="hidden" name="sms_code" id="sms_code"/>
                    </div>
                    <div class="col-md-6">
                        <label>Last name</label>
                        <input type="text" class="form-control" name="lname" placeholder="Last name" required>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>Photo</label>
                        <input type="file" name="avatar" class="croppie" crop-width="200" crop-height="200" accept="image/*" required>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email" placeholder="Email" required data-parsley-mailmdm="">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>No. HP</label>
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="0812XXXXXXXX atau 62812XXXXXXXX" required maxlength="13" minlength="10">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Password</label>
                        <input type="Password" class="form-control" name="password" data-parsley-required="true" data-parsley-minlength="6" data-parsley-error-message="Password is too short!" id="password" placeholder="Password">
                    </div>
                    <div class="col-md-6">
                        <label>Confirm Password</label>
                        <input type="Password" class="form-control" data-parsley-required="true" data-parsley-equalto="#password" data-parsley-error-message="Passwords don't Match!" placeholder="Confirm Password">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>Captcha</label>
                        <div class="g-recaptcha" data-sitekey="6LeI2tMUAAAAABy9pW8JWTwAXfS51GlbvnDzy-jC" data-callback="recaptchaCallback" data-expired-callback="exprecaptchaCallback"></div>
                        <span id='errorContainer'></span>
                    </div>
                </div>
            </div>
            <input id="capfiled" data-parsley-errors-container="#errorContainer" data-parsley-required="true" value="" type="text" style="display:none;">


            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <p class="pull-left m-t-5"><a href="" target="sign-in">Sign In?</a></p>
                        <button class="btn btn-primary pull-right"  type="submit">Create account</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    @endif
    <div class="forgot-password" style="display: none;">
        <h5 class="mb-30">Anda akan dikirimkan link reset password melalui email. <br>Silakan masukkan alamat email Anda..</h5>
        <form class="text-left simcy-form" action="<?=url("Auth@forgot");?>" method="POST" data-parsley-validate="" loader="true">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" placeholder="Email" required>
                        <input type="hidden" name="csrf-token" value="<?=htmlentities(csrf_token(), ENT_QUOTES, 'UTF-8');?>" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>Captcha</label>
                        <div class="g-recaptcha" data-sitekey="6LeI2tMUAAAAABy9pW8JWTwAXfS51GlbvnDzy-jC" data-callback="recaptchaCallback2" data-expired-callback="exprecaptchaCallback2"></div>
                        <span id='errorContainer2'></span>
                    </div>
                </div>
            </div>
            <input id="capfiled2" data-parsley-errors-container="#errorContainer2" data-parsley-required="true" value="" type="text" style="display:none;">

            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <p class="pull-left m-t-5"><a href="" target="sign-in">Login?</a></p>
                        <button class="btn btn-primary pull-right" type="submit">Kirim Email</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    @if ( env('NEW_ACCOUNTS') == "Enabled" )
    <div class="m-t-5">
        <a class="btn btn-block btn-primary-ish m-t-50 sign-up-btn" href="" target="sign-up">Create an account</a>
    </div>
    @endif
    <div class="copyright">
        <p class="text-center"><?=date("Y")?> &copy; <?=env("APP_NAME")?> | All Rights Reserved.</p>
    </div>
</div>

<div class="modal fade" id="m_otp" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="close-modal" data-dismiss="modal">&times;</div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">OTP</h4>
            </div>
            <div class="modal-body">
                <p>Isikan Kode Verifikasi dari SMS yang Anda terima.</p>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Kode OTP</label>
                            <input type="text" id="m_otp_code" class="form-control" placeholder="XXXXXX">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button id="b_otp" class="btn btn-warning" onclick="kirimulangotp(event)"></button>
                <button type="button" class="btn btn-primary copy-link" onclick="submitotp(event)">Submit</button>
            </div>
        </div>

    </div>
</div>

<!-- scripts -->
<script src="<?=url("");?>assets/js/jquery-3.3.1.min.js"></script>
<script src="<?=url("");?>assets/libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=url("");?>assets/js//jquery.slimscroll.min.js"></script>
<script src="<?=url("");?>assets/js/simcify.min.js"></script>

<!-- custom scripts -->
<script src="<?=url("");?>assets/js/app.js"></script>
<script src="<?=url("");?>assets/ariwa/jquery-validation/jquery.validate.min.js"></script>
<script src="<?=url("");?>assets/ariwa/jquery-validation/additional-methods.min.js"></script>

<script>
    var a_intvl = null;
    function setTimer(t) {
        $('#b_otp').prop('disabled', true);
        $('#b_otp').html('Loading..');
        var timer2 = "00:10";
        a_intvl = setInterval(function() {
            var timer = timer2.split(':');
            //by parsing integer, I avoid all extra string processing
            var minutes = parseInt(timer[0], 10);
            var seconds = parseInt(timer[1], 10);
            --seconds;
            minutes = (seconds < 0) ? --minutes : minutes;
            seconds = (seconds < 0) ? 59 : seconds;
            seconds = (seconds < 10) ? '0' + seconds : seconds;
            //minutes = (minutes < 10) ?  minutes : minutes;

            if (minutes < 0){
                clearInterval(a_intvl);
                $('#b_otp').prop('disabled', false);
                $('#b_otp').html('Kirim Ulang OTP');
            }else{
                $('#b_otp').prop('disabled', true);
                $('#b_otp').html('Kirim Ulang OTP ('+minutes + ':' + seconds+')');
            }
            timer2 = minutes + ':' + seconds;
        }, 1000);
    }

</script>
<script>
    // validator
    window.Parsley.addValidator('mailmdm', {
        validateString: function(value) {
            var m = value.split('@');
            if((m[1]=='mdmedia.co.id' || m[1]=='telkom.co.id' || m[1]=='metra.co.id' )){
                return true;
            }
            return false;
        },
        messages: {
            en: 'Email yang digunakan harus @mdmedia.co.id, @telkom.co.id atau @metra.co.id',
        }
    });

    function recaptchaCallback() {
        document.getElementById('capfiled').value = 'nonEmpty';
    }
    function exprecaptchaCallback() {
        document.getElementById('capfiled').value = '';
    }
    function recaptchaCallback2() {
        document.getElementById('capfiled2').value = 'nonEmpty';
    }
    function exprecaptchaCallback2() {
        document.getElementById('capfiled2').value = '';
    }

    $(function() {
        /*$('input[name="fname"]').val("Ari");
        $('input[name="lname"]').val("Wahyu");
        $('input[name="email"]').val("ari.wahyu@mdmedia.co.id");
        $('input[name="password"]').val("ariwa123");
        $('input[name="phone"]').val("6281216066929");*/
        $('#m_otp').on('hidden.bs.modal', function (e) {
            $('#sms_code').val('');
            $('#sms_id').val('');
        });
        $('#phone').on('input blur paste', function(){
            $(this).val($(this).val().replace(/\D/g, '').toString());
        });
    });

    function validasi(e) {
        e.preventDefault();
        if(confirm('No HP digunakan untuk Verifikasi. Apakah No HP yang Anda masukan sudah benar ?')){
            $.ajax({
                url:'<?=url("");?>validasi/otp',
                data:{ phone: $('#phone').val(), 'csrf-token': '<?=htmlentities(csrf_token(), ENT_QUOTES, 'UTF-8');?>' },
                type:'POST',
                dataType:"text",
                success:function(data){
                    $('#m_otp').modal('show');
                },
                error:function(data){

                }
            });
        }
    }

    $('#form_registrasi').submit(function(event) {
        event.preventDefault();
        var loader = false;
        if ($(this).attr("loader") === "true") {
            loader = true;
        }
        $(this).parsley().validate();
        if (($(this).parsley().isValid())) {
            if(!$('#m_otp').hasClass('in')){
                if(!confirm('No HP digunakan untuk Verifikasi. Apakah No HP yang Anda masukan sudah benar ?')){
                    return false;
                }
            }

            if (loader) {
                showLoader();
            }
            $.ajax({
                url: $(this).attr("action"),
                type: $(this).attr("method"),
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function (response) {
                    if (loader) {
                        hideLoader();
                    }
                    if (response.status === "error") {
                        toastr.error(response.message, response.title);
                    }else if(response.status === "otp"){
                        $('#sms_id').val(response.sms_id);
                        $('#m_otp').modal('show');
                        setTimer(1);
                    }else{
                        $('body').html('Loading..');
                        window.location.reload();
                    }
                },
                error: function (xhr, status, error) {
                    if (loader) {
                        hideLoader();
                    }
                    toastr.error(error, "Oops!");
                }
            });
        }
    });
    
    function submitotp(e) {
        e.preventDefault();
        $('#sms_code').val($('#m_otp_code').val());
        $('#form_registrasi').submit();
    }

    function kirimulangotp(e) {
        var indata = $('#form_registrasi').serializeArray();
        indata.splice(5,1);
        indata.splice(8,1);

        e.preventDefault();
        $('#b_otp').prop('disabled', true);
        $('#b_otp').html('Loading..');
        $.ajax({
            url:'<?=url("");?>validasi/otpulang',
            data:{ sms_id: $('#sms_id').val(), 'csrf-token': '<?=htmlentities(csrf_token(), ENT_QUOTES, 'UTF-8');?>', indata: indata },
            type:'POST',
            dataType:"json",
            success:function(response){
                if (response.status === "error") {
                    toastr.error(response.message, response.title);
                    return false;
                }
                $('#sms_id').val(response.sms_id);
                setTimer(1);
            },
            error:function(data){

            }
        });
    }

</script>

</body>

</html>
