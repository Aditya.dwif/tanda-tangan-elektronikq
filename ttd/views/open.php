<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ config('app.metadata')['title_desc'] }}">
    <meta name="author" content="{{ config('app.metadata')['title_desc'] }}">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=url("");?>uploads/app/{{ env('APP_ICON'); }}">
    <title>{{ $document->name }} | {{ config('app.metadata')['title'] }}</title>
    <!-- Ion icons -->
    <link href="<?=url("");?>assets/fonts/ionicons/css/ionicons.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=B612+Mono:400,400i,700|Charm:400,700|EB+Garamond:400,400i,700|Noto+Sans+TC:400,700|Open+Sans:400,400i,700|Pacifico|Reem+Kufi|Scheherazade:400,700|Tajawal:400,700&amp;subset=arabic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash|Cookie|Courgette|Dr+Sugiyama|Grand+Hotel|Great+Vibes|League+Script|Meie+Script|Miss+Fajardose|Niconne|Pacifico|Petit+Formal+Script|Rochester|Sacramento|Tangerine" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?=url("");?>assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/select2/css/select2.min.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
    <link href="<?=url("");?>assets/css/simcify.min.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <!-- Signer CSS -->
    <link href="<?=url("");?>assets/css/style.css" rel="stylesheet">
    <script src="<?=url("");?>assets/js/jscolor.js"></script>

    <style type="text/css">
        .img-header, .thumbnail > img, .thumbnail a > img, .carousel-inner > .item > img, .carousel-inner > .item > a > img {
            display: block;
            max-width: 100%;
            height: 55px;
            margin-top: -10px;
        }
    </style>
</head>

<body>

    <!-- header start -->
    <header>
        <!-- Hambager -->
        <!-- Hambager -->
        <div class="humbager">
            <i class="ion-navicon-round"></i>
        </div>
        <!-- logo -->
        <div class="logo">
            <a href="<?=url("");?>">
                <img src="<?=url("");?>uploads/app/{{ env('APP_LOGO'); }}" class="img-header">
            </a>
        </div>

        <!-- top right -->
        <ul class="nav header-links pull-right">
            <li class="m-o">
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="ion-arrow-down-b"></i> More </button>
                    <ul class="dropdown-menu public-actions">
                        <li><a href="{{ url('') }}uploads/files/{{ $document->filename }}" download="{{ $document->name.'.'.$document->extension }}"><i class="ion-ios-cloud-download-outline"></i><span> Download</span></a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </header>


    <div class="content mb-30" style="margin: 0 0 0 0">
        <div class="row">
            <div class="col-md-8 put-center">
                <div class="page-title">
                    <h3>{{ $document->name }}</h3>
                </div>
                <div class="light-card document">
                    <div class="signer-document">
                        @if ( $document->extension == "pdf" )
                        <!-- open PDF docements -->
                        <div class="document-pagination">
                            <div class="pull-left">
                                <button id="prev" class="btn btn-default btn-round"><i class="ion-ios-arrow-left"></i></button>
                                <button id="next" class="btn btn-default btn-round"><i class="ion-ios-arrow-right"></i></button>
                                <span class="text-muted ml-15">Page <span id="page_num">0</span> of <span id="page_count">0</span></span>
                            </div>
                            <div class="pull-right">
                                <button class="btn btn-default btn-round btn-zoom" zoom="plus"><i class="ion-plus"></i></button>
                                <button class="btn btn-default btn-round btn-zoom" zoom="minus"><i class="ion-minus"></i></button>

                            </div>
                        </div>
                        <div class="document-load">
                            <div class="loader-box"><div class="circle-loader"></div></div>
                        </div>
                        <div class="document-error">
                            <i class="ion-android-warning text-danger"></i>
                            <p class="text-muted"><strong>Oops! </strong> <span class="error-message"> Something went wrong.</span></p>
                        </div>
                        <div class="text-center">
                            <div class="document-map"></div>
                            <canvas id="document-viewer"></canvas>
                        </div>
                        @else
                        <iframe src='https://view.officeapps.live.com/op/embed.aspx?src={{ env("APP_URL") }}/uploads/files/{{ $document->filename }}' width='100%' height='1000px' frameborder='0'></iframe>
                        @endif
                    </div>
                </div>
            </div>


        </div>
    </div>
    <!-- footer -->
    <p class="text-muted text-center mb-30"> Powered by <?=env("APP_NAME")?> | <?=date("Y")?> &copy; <?=env("APP_NAME")?> | All Rights Reserved. </p>


    <!-- scripts -->
    <script src="<?=url("");?>assets/js/jquery-3.2.1.min.js"></script>
    <script src="<?=url("");?>assets/libs/dropify/js/dropify.min.js"></script>
    <script src="<?=url("");?>assets/libs/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=url("");?>assets/js/simcify.min.js"></script>
    <script src="<?=url("");?>assets/libs/html2canvas/html2canvas.js"></script>
    <script src="<?=url("");?>assets/libs/clipboard/clipboard.min.js"></script>
    <script src="<?=url("");?>assets/libs/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?=url("");?>assets/libs/select2/js/select2.min.js"></script>
    <script src="<?=url("");?>assets/libs/tagsinput/bootstrap-tagsinput.js"></script>
    <script src="<?=url("");?>assets/js//jquery.slimscroll.min.js"></script>
    <script src="<?=url("");?>assets/libs/jcanvas/jcanvas.min.js"></script>
    <script src="<?=url("");?>assets/libs/jcanvas/signature.min.js"></script>
    <script src="<?=url("");?>assets/libs/jcanvas/editor.min.js"></script>
    <script src="<?=url("");?>assets/js/touch-punch.min.js"></script>
    <script src="<?=url("");?>assets/js/pdf.js"></script>
    <script type="text/javascript">
        var url = '<?=url("");?>uploads/files/{{ $document->filename }}',
              isTemplate = '{{ $document->is_template }}',
              signDocumentUrl = '<?=url("Guest@sign");?>',
              auth = false;
              createTemplateUrl = sendRequestUrl = postChatUrl = settingsPage = saveFieldsUrl = getChatUrl = deleteFieldsUrl = countNotificationsUrl = '',
              baseUrl = '<?=url("");?>',
              loginPage = '<?=url("Auth@get");?>';
              document_key = '{{ $document->document_key }}';
        PDFJS.workerSrc = '<?=url("");?>assets/js/pdf.worker.min.js';

        @if ( is_object($request) && $request->status == "Pending" )
        var signingKey = '{{ $request->signing_key }}';
        var requestPositions = {{ $requestPositions }};
        var requestWidth = {{ $requestWidth }};
        @else
        var signingKey = '';
        @endif


    </script>
    <!-- custom scripts -->
    <script src="<?=url("");?>assets/js/app.js"></script>
    <script src="<?=url("");?>assets/js/signer.js"></script>
    <script src="<?=url("");?>assets/js/render.js"></script>
</body>

</html>
