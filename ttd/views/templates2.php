<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ config('app.metadata')['title_desc'] }}">
    <meta name="author" content="{{ config('app.metadata')['title_desc'] }}">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=url("");?>uploads/app/{{ env('APP_ICON'); }}">
    <title>Dokumen | {{ config('app.metadata')['title'] }}</title>
    <!-- Ion icons -->
    <link href="<?=url("");?>assets/fonts/ionicons/css/ionicons.css" rel="stylesheet">
    <link href="<?=url("");?>assets/ariwa/font-awesome/css/all.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?=url("");?>assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/select2/css/select2.min.css" rel="stylesheet">
    <link href="<?=url("");?>assets/css/simcify.min.css" rel="stylesheet">
    <!-- Signer CSS -->
    <link href="<?=url("");?>assets/css/style.css" rel="stylesheet">

    <script src="<?=url("");?>assets/js/jquery-3.2.1.min.js"></script>
    <style>
        .dataTables_filter {
            float: left !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button {
            padding : 0 !important;
            margin-left: 0 !important;
            display: inline !important;
            border: 0 !important;
            font-size: 10px !important;
        }
        .dataTables_wrapper .dataTables_paginate .pagination {
            margin: 0 !important;
        }
        .dataTables_filter {
            float: left !important;
        }
    </style>

</head>

<body>

<!-- header start -->
{{ view("includes/header", $data); }}

<!-- sidebar -->
{{ view("includes/sidebar", $data); }}

<div class="content">
    <div class="page-title templates-page" style="overflow:visible;">
        <div class="row">
            <div class="col-md-6 col-xs-6">
                <h3>Templates</h3>
                <p class="breadcrumbs text-muted">Manage your templates here.</p>
            </div>
            <div class="col-md-6 col-xs-6 text-right page-actions">
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="ion-arrow-down-b"></i> New Template </button>
                    <ul class="dropdown-menu" role="menu">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" data-toggle="modal" data-target="#uploadFile" data-backdrop="static" data-keyboard="false">
                                <i class="ion-ios-cloud-upload-outline"></i> <span>Upload File</span></a></li>
                        <?php if(!empty(env("DROPBOX_APP_KEY"))){ ?>
                            <li role="presentation" class="divider"></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" data-toggle="modal" data-target="#dropbox" data-backdrop="static" data-keyboard="false">
                                    <i class="ion-social-dropbox-outline"></i> <span>Dropbox</span></a></li>
                        <?php } ?>
                        <?php if(!empty(env("GOOGLE_CLIENT_ID")) && !empty(env("GOOGLE_API_KEY"))){ ?>
                            <li role="presentation" class="divider"></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" data-toggle="modal" data-target="#google-drive" data-backdrop="static" data-keyboard="false">
                                    <i class="ion-social-google-outline"></i> <span>Google Docs</span></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table" id="table_templates">
                <thead>
                <tr>
                    <th>Type</th>
                    <th>File</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>

</div>

<!-- footer -->
{{ view("includes/footer"); }}

<script src="<?=url("");?>assets/libs/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
    var baseUrl = '<?=url("");?>';
</script>

<script src="<?=url("");?>assets/ariwa/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?=url("");?>assets/ariwa/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="<?=url("");?>assets/js/ariwa/templates.js"></script>

<script src="<?=url("");?>assets/js/simcify.min.js"></script>
<script src="<?=url("");?>assets/js/app.js"></script>
</body>

</html>
